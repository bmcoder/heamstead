<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php' ?>
    <link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css">
</head>

<body class="bg-default">

<div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                        <h1 class="text-white">Installation</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-7 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-body px-lg-5 py-lg-5 custom-wizard">

                        <form id="horizontal-wizard" method="post" enctype="multipart/form-data" action="#">
                            <h3>Account</h3>
                            <section class="p-2 mt-4">
                                <div class="form-group">
                                    <label for="userName">Email Address *</label>
                                    <input type="email" class="form-control required email" name="userName" id="userName">
                                </div>
                                <div class="form-group">
                                    <label for="password">Password *</label>
                                    <input id="password" name="password" type="password" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label for="confirm">Confirm Password *</label>
                                    <input id="confirm" name="confirm" type="password" class="form-control required">
                                </div>
                            </section>
                            <h3>Database</h3>
                            <section class="p-2 mt-4">
                                <div class="form-group">
                                    <label for="dbhost">Host *</label>
                                    <input type="text" class="form-control required" name="dbhost" id="dbhost" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="dbuser">User Name *</label>
                                    <input type="text" class="form-control required" name="dbuser" id="dbuser" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="dbpassword">Password *</label>
                                    <input type="text" class="form-control required" name="dbpassword" id="dbpassword" placeholder="">
                                </div>
                                <div class="form-group">
                                    <label for="dbname">Database Name *</label>
                                    <input type="text" class="form-control required" name="dbname" id="dbname" placeholder="">
                                </div>
                            </section>
                            <h3>Google API</h3>
                            <section class="p-2 mt-4">
                                <div class="form-group">
                                    <label for="client_secret">JSON File *</label>
                                    <input type="file" class="form-control required" name="client_secret" id="client_secret" placeholder="">
                                    <a href="https://ga.deadlockinfotech.com/GA-Create-Client-JSON.pdf" style="font-size: 13px;font-weight: 500;" target="_blank">*Follow this for create your client json file</a>
                                </div>
                                <div class="custom-control custom-checkbox checkbox-primary form-check">
                                    <input type="checkbox" class="custom-control-input required" id="acceptTerms" name="acceptTerms">
                                    <label class="custom-control-label" for="acceptTerms">I agree with the Terms and Conditions.</label>
                                </div>
                            </section>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="py-5" id="footer-main">
    <div class="container">
        <div class="row align-items-center justify-content-xl-between">
            <div class="col-xl-12">
                <div class="copyright text-center text-muted">
                    Copyright &copy; <?php echo date('Y') ?> <a href="https://rajodiya.com" class="font-weight-bold ml-1" target="_blank">Rajodiya Infotech</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php require_once 'footer.php' ?>
<script src="assets/vendor/jquery-validation/jquery.validate.min.js"></script>
<script src="assets/vendor/jquery-steps/jquery.steps.min.js"></script>
<script src="assets/js/components/horizontal-wizard-init.js"></script>
<script src="assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
</body>

</html>
