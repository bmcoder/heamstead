<?php
require_once 'connection.php';
if(file_exists("./config/database.php") && file_exists("./config/client_secret_native.json"))
{
    $retuen['message']   = "Database & GA already config.";
    $retuen['isSuccess'] = false;
    echo json_encode($retuen);
    exit();
}
if(!empty($_POST) && !empty($_FILES))
{
    $retuen = [];
    $con    = new mysqli($_POST['dbhost'], $_POST['dbuser'], $_POST['dbpassword'], $_POST['dbname']);
    if($con->connect_errno)
    {
        $retuen['message']   = "Connect failed: " . $con->connect_error;
        $retuen['isSuccess'] = false;
        echo json_encode($retuen);
        exit();
    }
    else
    {
        $url = str_replace('install.php', '', $_SERVER['HTTP_REFERER']);

        $my_file = __DIR__ . '/config/';
        // if(chmod($my_file, 0777))
        {
            $my_file .= 'database.php';
            if($handle = fopen($my_file, 'w'))
            {
                $data = '<?php define("DBHOST","' . $_POST['dbhost'] . '");';
                $data .= ' define("DBUSER","' . $_POST['dbuser'] . '");';
                $data .= ' define("DBPASS","' . $_POST['dbpassword'] . '");';
                $data .= ' define("DBNAME","' . $_POST['dbname'] . '");';
                $data .= ' define("APP_URL","' . $url . '"); ?>';
                fwrite($handle, $data);

                $con->query("CREATE DATABASE IF NOT EXISTS `" . $_POST['dbname'] . "`;");
                $con->query(
                    "CREATE TABLE `" . $_POST['dbname'] . "`.`admin` (
							  `username` varchar(50) PRIMARY KEY,
							  `name` varchar(100) NOT NULL,
							  `password` varchar(50) NOT NULL,
							  `profile_pic` varchar(100) DEFAULT NULL
							)"
                );
                $con->query("INSERT INTO `" . $_POST['dbname'] . "`.`admin` VALUES('" . $_POST['userName'] . "','Admin','" . $_POST['password'] . "','')");

                $con->query(
                    "CREATE TABLE `" . $_POST['dbname'] . "`.`site` (
							  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
							  `name` varchar(50) NOT NULL,
                              `color` varchar(50) NOT NULL,
                              `icon_class` varchar(60) DEFAULT NULL,
							  `account_id` varchar(50) NOT NULL,
							  `property_id` varchar(50) NOT NULL,
							  `project_id` varchar(50) NOT NULL,
							  `accessToken` varchar(1000) NOT NULL,
							  `refreshToken` varchar(500) NOT NULL,
							  `timeframe` varchar(100) NOT NULL DEFAULT 'today',
                              `graph` varchar(250) NOT NULL DEFAULT 'ga:users',
                              `graph_type` varchar(10) NOT NULL DEFAULT 'line',
                              `graph_color` varchar(50) NOT NULL DEFAULT '#172b4d',
                              `top_left` varchar(250) NOT NULL DEFAULT 'ga:users',
                              `top_right` varchar(250) NOT NULL DEFAULT 'ga:avgSessionDuration',
                              `bottom_left` varchar(250) NOT NULL DEFAULT 'ga:avgSessionDuration',
                              `bottom_right` varchar(250) NOT NULL DEFAULT 'ga:bounceRate',
							  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
							  `update_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
							)"
                );

                $con->query(
                    "CREATE TABLE `" . $_POST['dbname'] . "`.`widget` (
                                `id` int(11) PRIMARY KEY AUTO_INCREMENT,
                                `site_id` int NOT NULL,
                                `title` varchar(100) DEFAULT NULL,
                                `metrics_1` varchar(100) NOT NULL,
                                `metrics_2` varchar(100) NOT NULL
							)"
                );


                if($_FILES['client_secret']['error'] == 0)
                {
                    if($_FILES['client_secret']['type'] == "application/json")
                    {
                        move_uploaded_file($_FILES['client_secret']['tmp_name'], __dir__ . '/config/client_secret_native.json');
                        $ga = new GA();
                        if(empty($jsonError = $ga->checkJSONFile()))
                        {
                            $retuen['isSuccess'] = true;
                            echo json_encode($retuen);
                            exit();
                        }
                        else
                        {
                            $retuen['isSuccess'] = false;
                            $retuen['message']   = $jsonError;
                            echo json_encode($retuen);
                            unlink(__dir__ . '/config/client_secret_native.json');
                            exit();
                        }
                    }
                    else
                    {
                        $retuen['message']   = 'File type is invalid. JSON file is only allow.';
                        $retuen['isSuccess'] = false;
                        echo json_encode($retuen);
                        exit();
                    }
                }
                else
                {
                    $retuen['message']   = 'Error in file upload.';
                    $retuen['isSuccess'] = false;
                    echo json_encode($retuen);
                    exit();
                }
            }
            else
            {
                $retuen['message']   = 'Cannot open file: ' . $my_file;
                $retuen['isSuccess'] = false;
                echo json_encode($retuen);
                exit();
            }
        }
        //		 else
        //		 {
        //		 	$retuen['message'] = 'Cannot change file permission.';
        //		 	$retuen['isSuccess'] = false;
        //		 	echo json_encode($retuen);
        //		 	exit();
        //		 }
    }
}
?>
