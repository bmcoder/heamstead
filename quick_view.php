<?php
require_once 'connection.php';
$db = new DB();
$ga = new GA();
require_once 'admin_security.php';

if(isset($_GET['site']))
{
    $site = $db->getAccessToken(['name' => $_GET['site']]);
    if($site['isSuccess'])
    {
        $site = $site['objSite'];
    }
    else
    {
        $error = $site['message'];
    }
}

$arrTimeframe = [
    'today' => 'Today',
    'yesterday' => 'Yesterday',
    '7daysAgo' => 'Last 7 days',
    '15daysAgo' => 'Last 15 days',
    '30daysAgo' => 'Last 30 days',
];

$arrNumber = [
    'realtime' => 'Realtime Visitors',
    'ga:users' => 'Visitors',
];

$arrGraph = [
    "ga:avgSessionDuration" => "Session Time",
    'ga:users' => 'Users',
    "ga:sessions" => "Sessions",
    "ga:newUsers" => "New Users",
    "ga:pageviews" => "Page Views",
    "ga:bounceRate" => "Bounce Rate",
];

$arrTopLeft = [
    "ga:avgSessionDuration" => "Session Time",
    "ga:users" => "Users",
    "ga:sessions" => "Sessions",
    "ga:newUsers" => "New Users",
    "ga:pageviews" => "Page Views",
    "ga:bounceRate" => "Bounce Rate",
    "ga:percentNewSessions" => "New Session %",
    "ga:goalCompletionsAll" => "All goals",
    "ga:goalConversionRateAll" => "All Goals CR %",
    "ga:goalValueAll" => "All goals value",
    "ga:totalEvents" => "Total Events",
    "ga:transactions" => "All transactions",
    "ga:transactionRevenue" => "All transaction revenue",
    "ga:transactionRevenuePerSession" => "Avg. revenue per session",
];

$arrTopRight = [
    "ga:avgSessionDuration" => "Session Time",
    "ga:users" => "Users",
    "ga:sessions" => "Sessions",
    "ga:newUsers" => "New Users",
    "ga:pageviews" => "Page Views",
    "ga:bounceRate" => "Bounce Rate",
    "ga:percentNewSessions" => "New Session %",
    "ga:goalCompletionsAll" => "All goals",
    "ga:goalConversionRateAll" => "All Goals CR %",
    "ga:goalValueAll" => "All goals value",
    "ga:totalEvents" => "Total Events",
    "ga:transactions" => "All transactions",
    "ga:transactionRevenue" => "All transaction revenue",
    "ga:transactionRevenuePerSession" => "Avg. revenue per session",
];

$arrBottomLeft = [
    "ga:avgSessionDuration" => "Session Time",
    "ga:users" => "Users",
    "ga:sessions" => "Sessions",
    "ga:newUsers" => "New Users",
    "ga:pageviews" => "Page Views",
    "ga:bounceRate" => "Bounce Rate",
    "ga:percentNewSessions" => "New Session %",
    "ga:goalCompletionsAll" => "All goals",
    "ga:goalConversionRateAll" => "All Goals CR %",
    "ga:goalValueAll" => "All goals value",
    "ga:totalEvents" => "Total Events",
    "ga:transactions" => "All transactions",
    "ga:transactionRevenue" => "All transaction revenue",
    "ga:transactionRevenuePerSession" => "Avg. revenue per session",
];

$arrBottomRight = [
    "ga:avgSessionDuration" => "Session Time",
    "ga:users" => "Users",
    "ga:sessions" => "Sessions",
    "ga:newUsers" => "New Users",
    "ga:pageviews" => "Page Views",
    "ga:bounceRate" => "Bounce Rate",
    "ga:percentNewSessions" => "New Session %",
    "ga:goalCompletionsAll" => "All goals",
    "ga:goalConversionRateAll" => "All Goals CR %",
    "ga:goalValueAll" => "All goals value",
    "ga:totalEvents" => "Total Events",
    "ga:transactions" => "All transactions",
    "ga:transactionRevenue" => "All transaction revenue",
    "ga:transactionRevenuePerSession" => "Avg. revenue per session",
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="assets/vendor/jvectormap-next/jquery-jvectormap.css">
    <link rel="stylesheet" href="assets/css/custom.css">
</head>

<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>

    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Quick View</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <?php
                    $arrSite = $db->select('site');
                    if($arrSite['total_record'])
                    {
                        while($row = $arrSite['rs']->fetch_object())
                        { ?>
                          <?php if($curr_user->type == 1){ ?>
                            <div class="col-md-12 col-lg-6 col-xl-4" id="<?php echo $row->id; ?>_wrapper">
                                <div class="card">
                                    <div id="rec_<?php echo $row->id; ?>">
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-6">
                                                    <h3 class="mb-0"><?php echo $row->name; ?></h3>
                                                </div>
                                                <div class="col-6 text-right">
                                                    <a href="#" class="site_name" data-site="<?php echo $row->name ?>" data-siteid="<?php echo $row->id; ?>"><i class="ni ni-settings-gear-65"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body bg-white">
                                            <div class="chart" id="card_chart_div">
                                                <div class="text-center" id="chart_loader_<?php echo $row->id; ?>">
                                                    <img src="assets/img/loader.gif"/>
                                                </div>
                                                <canvas id="chart-<?php echo $row->id; ?>" class="chart-canvas"></canvas>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row pb-3">
                                                <div class="col-md-6">
                                                    <h6 class="surtitle" id="top_left_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="top_left_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <h6 class="surtitle" id="top_right_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="top_right_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6 class="surtitle" id="bottom_left_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="bottom_left_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <h6 class="surtitle" id="bottom_right_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="bottom_right_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              <?php }else{
                                    if($curr_user->username == $row->user)
                                        {
                                ?>
                                
                                
                                 <div class="col-md-12 col-lg-6 col-xl-4" id="<?php echo $row->id; ?>_wrapper">
                                <div class="card">
                                    <div id="rec_<?php echo $row->id; ?>">
                                        <div class="card-header">
                                            <div class="row">
                                                <div class="col-6">
                                                    <h3 class="mb-0"><?php echo $row->name; ?></h3>
                                                </div>
                                                <div class="col-6 text-right">
                                                    <a href="#" class="site_name" data-site="<?php echo $row->name ?>" data-siteid="<?php echo $row->id; ?>"><i class="ni ni-settings-gear-65"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body bg-white">
                                            <div class="chart" id="card_chart_div">
                                                <div class="text-center" id="chart_loader_<?php echo $row->id; ?>">
                                                    <img src="assets/img/loader.gif"/>
                                                </div>
                                                <canvas id="chart-<?php echo $row->id; ?>" class="chart-canvas"></canvas>
                                            </div>
                                        </div>
                                        <div class="card-footer">
                                            <div class="row pb-3">
                                                <div class="col-md-6">
                                                    <h6 class="surtitle" id="top_left_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="top_left_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <h6 class="surtitle" id="top_right_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="top_right_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <h6 class="surtitle" id="bottom_left_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="bottom_left_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                                <div class="col-md-6 text-right">
                                                    <h6 class="surtitle" id="bottom_right_id_<?php echo $row->id; ?>">-</h6>
                                                    <h5 class="h3 mb-0" id="bottom_right_value_<?php echo $row->id; ?>">-</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                                
                                 <?php
                                        }
                                    }?>
                                       
                                
                                    
                            <?php
                        }
                    }
                    ?>
                </div>
            </section>
        </div>
    </div>
</div>

<div class="modal fade" id="site_card" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-secondary" role="document">
        <div class="modal-content">
            <div class="modal-header pb-2">
                <h3 class="site_title modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-dark">&times;</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <form method="POST" name="card_post" id="card_post">
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="timeframe">Timeframe</label>
                            <select class="form-control selectric" id="timeframe" name="timeframe">
                                <?php foreach($arrTimeframe as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="form-gorup col-6">
                            <label for="graph">Graph</label>
                            <select class="form-control selectric" id="graph" name="graph">
                                <?php foreach($arrGraph as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-gorup col-6">
                            <label for="graph_type">Graph Type</label>
                            <select class="form-control selectric" id="graph_type" name="graph_type">
                                <option value="line">Line</option>
                                <option value="bar">Bar</option>
                            </select>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="form-gorup col-6">
                            <label for="top_left">Top Left</label>
                            <select class="form-control selectric" id="top_left" name="top_left">
                                <?php foreach($arrTopLeft as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-gorup col-6">
                            <label for="top_right">Top Right</label>
                            <select class="form-control selectric" id="top_right" name="top_right">
                                <?php foreach($arrTopRight as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row pb-3">
                        <div class="form-gorup col-6">
                            <label for="bottom_left">Bottom Left</label>
                            <select class="form-control selectric" id="bottom_left" name="bottom_left">
                                <?php foreach($arrBottomLeft as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-gorup col-6">
                            <label for="bottom_right">Bottom Right</label>
                            <select class="form-control selectric" id="bottom_right" name="bottom_right">
                                <?php foreach($arrBottomRight as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="row pb-4">
                        <div class="col-12">
                            <label>Select Chart color</label>
                        </div>
                        <div class="btn-group btn-group-toggle btn-group-colors event-tag mb-0" data-toggle="buttons">
                            <label class="btn bg-info g_clr active mx-4">
                                <input type="radio" name="graph_color" value="#44cdee">
                            </label>
                            <label class="btn bg-warning g_clr mx-4">
                                <input type="radio" name="graph_color" value="#f66340">
                            </label>
                            <label class="btn bg-danger g_clr mx-4">
                                <input type="radio" name="graph_color" value="#f5365c">
                            </label>
                            <label class="btn bg-success g_clr mx-4">
                                <input type="radio" name="graph_color" value="#40ce89">
                            </label>
                            <label class="btn bg-default g_clr mx-4">
                                <input type="radio" name="graph_color" value="#172b4d">
                            </label>
                            <label class="btn bg-primary g_clr mx-4">
                                <input type="radio" name="graph_color" value="#5e71e4">
                            </label>
                        </div>
                    </div>
                    <div class="row text-right">
                        <div class="form-gorup col-12">
                            <button type="button" class="btn btn-primary submit_view">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var url = "<?php echo APP_URL . 'getChart.php' ?>";
</script>
<?php require_once 'footer.php' ?>

<script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="assets/js/components/init/chart-init.js"></script>
<script src="assets/js/chartjs-init.js"></script>

<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
</body>
</html>