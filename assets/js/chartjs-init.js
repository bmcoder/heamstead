// -----------------------------------------------------------------------------
// Title: Demo code for Chart.js
// Location: charts.chartjs.html
// IDs: #chartjs_lineChart,#chartjs_barChart,#chartjs_radarChart,#chartjs_polarChart,#chartjs_pieChart,#chartjs_doughnutChart
// Dependency File(s): assets/vendor/chart.js/dist/Chart.bundle.min.js
// -----------------------------------------------------------------------------

var arrChartRec = [];
var chart1;
var chart2;

$(document).ready(function () {
    if ($(".selectric").length) {
        $(".selectric").selectric({
            disableOnMobile: false,
            nativeOnMobile: false
        });
    }
});

(function (window, document, $, undefined) {
    "use strict";
    $(function () {

        if ($('#chart_user_type').length) {
            get_chart_user_type();
        }
        if ($('#usersChart').length) {
            get_usersChart();
        }
        if ($('#bounceRateChart').length) {
            get_bounceRateChart();
        }
        if ($('#sessionDuration').length) {
            get_sessionDuration();
        }
        if ($('.site_name').length) {
            $('.site_name').each(function (data) {
                var siteName = $(this).attr('data-site');
                var siteid = $(this).attr('data-siteid');
                loadQuickView(siteName, siteid);
            });
        }
        // if ($('.widget_name').length) {
        //     setTimeout(function () {
        //         $('.widget_name').each(function (data) {
        //             var siteName = $(this).attr('data-site');
        //             var id = $(this).attr('data-id');
        //             loadWidgetView(siteName, id);
        //         });
        //     }, 300);
        //     return false;
        // }

    });
    $(document).on("click", "#card_user_type .chart_duration .nav-item .nav-link", function () {
        get_chart_user_type();
    });
    $(document).on("click", "#card_custom .chart_duration .nav-item .nav-link", function () {
        get_chart_custom();
    });
    $(document).on("click", "#custom_form button", function () {
        get_chart_custom();
    });
    $(document).on("click", ".submit_view", function () {
        $('#card_post').submit();
    });

    // Widget Page Code
    $(document).on("click", '.card_open', function () {
        $('#title').val('');
        $('#metrics_1').prop('selectedIndex', 0).selectric('refresh');
        $('#metrics_2').prop('selectedIndex', 0).selectric('refresh');
    });

    $(document).on("click", '.submit_widget', function () {
        $('#no-widget').hide();
        $('#widget_post').submit();
    });
    $(document).on("click", '.widget_name', function () {
        var site = $(this).attr('data-site');
        var id = $(this).attr('data-id');

        var data = {
            'site': site,
            'id': id,
            'actionType': 'get_widget_for_update'
        };

        $.post(url, data, function (responce) {
            responce = JSON.parse(responce);
            if (responce.is_success) {
                $('#edit_title').val(responce.data.title);
                $('#edit_metrics_1 option[value="' + responce.data.metrics_1 + '"]').attr('selected', 'selected');
                $('#edit_metrics_2 option[value="' + responce.data.metrics_2 + '"]').attr('selected', 'selected');
                $('#widget_id').val(id);
                $('select').selectric('refresh');
                setTimeout(function () {
                    $("#card_widget_edit").modal("show");
                }, 300);

            }
        });

    });
    $(document).on('click', '.update_widget', function () {
        $('#widget_update').submit();
    });
    $(document).on('click', '.delete_widget', function () {

        var widget_id = $('#widget_id').val();
        var data = {
            'site': $('#site').val(),
            'widget_id': widget_id,
            'actionType': 'remove_widget'
        };

        $.post(url, data, function (responce) {
            responce = JSON.parse(responce);
            if (responce.is_success) {
                $('.widget_name').each(function (data) {
                    if ($(this).attr('data-id') == widget_id) {
                        $(this).parents('.w_head' + widget_id).addClass('d-none');
                        if ($('#card_widget_edit').is(':visible')) {
                            $("#card_widget_edit").modal("hide");
                        }
                    }
                });
            }
        });
    });

    $('#widget_post').on('submit', function (e) {
        e.preventDefault();
        var site = $('#site').val();

        var data = {
            'site': site,
            'formData': $(this).serialize(),
            'actionType': 'set_widget_data'
        };

        $.post(url, data, function (responce) {
            responce = JSON.parse(responce);
            if (responce.is_success) {
                $("#card_widget").modal("hide");
                var html = '<div class="col-md-3 w_head' + responce.widget_id + '">' +
                    '<div class="card">' +
                    '<div class="card-header">' +
                    '<div class="row">' +
                    '<div class="col-6">' + responce.title + '</div>' +
                    '<div class="col-6 text-right">' +
                    '<a href="#" class="widget_name" data-site="' + site + '" data-id="' + responce.widget_id + '"><i class="ni ni-settings-gear-65"></i></a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="card-body">' +
                    '<div class="row">' +
                    '<div class="col-6 text-center">' +
                    '<p class="text-lg font-weight-bold mb-1" id="metrics_1' + responce.widget_id + '">0</p>' +
                    '<p class="text-sm mb-0" id="metrics_1' + responce.widget_id + '_key">-</p>' +
                    '</div>' +
                    '<div class="col-6 text-center">' +
                    '<p class="text-lg font-weight-bold mb-1" id="metrics_2' + responce.widget_id + '">0</p>' +
                    '<p class="text-sm mb-0" id="metrics_2' + responce.widget_id + '_key">-</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
                $('#load_wrapper').append(html);
                loadWidgetView(site, responce.widget_id);
            }
        })
    });
    $('#widget_update').on('submit', function (e) {
        e.preventDefault();
        var site = $('#site').val();
        var data = {
            'site': site,
            'formData': $(this).serialize(),
            'actionType': 'update_widget_data'
        };

        $.post(url, data, function (responce) {
            responce = JSON.parse(responce);
            if (responce.is_success) {
                loadWidgetView(site, responce.widget_id);
            }
        });
    });
    // end widget page code

    $('#card_post').on('submit', function (e) {
        e.preventDefault();
        var siteName = $(this).attr('data-formid');
        var frmData = $(this).serialize();
        submit_cardData(siteName, frmData);
    });

    $(document).on("click", ".site_name", function () {
        var site_name = $(this).attr('data-site');
        get_cardData(site_name);
    });

    if ($('#world-map').length > 0) {
        get_world_map();
    }
    if ($('#live_users').length > 0) {
        get_live_user();
    }
    if ($('#active_pages').length > 0) {
        get_active_pages();
    }
    if ($('#session_by_device').length > 0) {
        get_session_by_device();
    }
    //    setInterval(get_live_user, 30000);

    if ($('#date_duration').length > 0) {
        $(document).on('change', '#date_duration', function () {
            // Analytics Page
            if ($('.analytics_data').length) {
                getAnalyticsData();
            }

            // Audience Page
            if ($('.audience_data').length) {
                getAudienceData();
            }

            // Pages Page
            if ($('.pages_data').length) {
                getPagesData();
            }

            // SEO Page
            if ($('.seo_data').length) {
                getSEOData();
            }

            // Widget Page
            if ($('#load_wrapper').length) {
                setTimeout(function () {
                    $('.widget_name').each(function (data) {
                        var siteName = $(this).attr('data-site');
                        var id = $(this).attr('data-id');
                        loadWidgetView(siteName, id);
                    });
                }, 300);
            }
        });
    }

    // Analytics Page
    if ($('.analytics_data').length > 0) {
        $('.analytics_data').on('click', function (e) {
            makeButtonActive($(this), 'analytics_data');
            getAnalyticsData();
        });
    }
    if ($('.card-metrics').length > 0) {
        $('.card-metrics').on('click', function (e) {
            e.preventDefault();
            makeCardActive($(this), 'card-metrics');
            drawUsagesChart($(this).attr('id'));
        });
    }
    // End

    // Audience Page
    if ($('.audience_data').length > 0) {
        $('.audience_data').on('click', function (e) {
            makeButtonActive($(this), 'audience_data');
            getAudienceData();
        });
    }
    if ($('.card-audience').length > 0) {
        $('.card-audience').on('click', function (e) {
            e.preventDefault();
            makeCardActive($(this), 'card-audience');
            drawUsagesChart($(this).attr('id'));
        });
    }
    // End

    // Pages Page
    if ($('.pages_data').length > 0) {
        $('.pages_data').on('click', function (e) {
            makeButtonActive($(this), 'pages_data');
            getPagesData();
        });
    }
    if ($('.card-pages').length > 0) {
        $('.card-pages').on('click', function (e) {
            e.preventDefault();
            makeCardActive($(this), 'card-pages');
            drawUsagesChart($(this).attr('id'));
        });
    }
    // End

    // SEO Page
    if ($('.seo_data').length > 0) {
        $('.seo_data').on('click', function (e) {
            makeButtonActive($(this), 'seo_data');
            getSEOData();
        });
    }
    if ($('.card-seo').length > 0) {
        $('.card-seo').on('click', function (e) {
            e.preventDefault();
            makeCardActive($(this), 'card-seo');
            drawUsagesChart($(this).attr('id'));
        });
    }
    // End

})(window, document, window.jQuery);

// Widget Page Data
function loadWidgetView(site, id) {
    var date = $('#date_duration').val();
    var data = {
        'site': site,
        'date': date,
        'widget_id': id,
        'actionType': 'get_widget_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        $('#title' + id).html(responce.title);
        $.each(responce.data, function (key, val) {
            var k = Object.keys(val)[0];
            var v = Object.values(val)[0];
            $('#' + key + '' + id + '_key').html(k);
            $('#' + key + '' + id).html(v);
        });

        if ($('#card_widget_edit').is(':visible')) {
            $('#edit_metrics_1').removeAttr('selected');
            $('#edit_metrics_2').removeAttr('selected');
            $('select').selectric('refresh');
            $("#card_widget_edit").modal("hide");
        }
    });
}

// end Widget Page Data

// Quick View Page Data
function get_cardData(site) {
    var data = {
        'site': site,
        'actionType': 'get_card_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {

            // Reset form
            $('#timeframe option[value="' + responce.data.timeframe + '"]').removeAttr('selected');
            $('#graph option[value="' + responce.data.graph + '"]').removeAttr('selected');
            $('#graph_type option[value="' + responce.data.graph_type + '"]').removeAttr('selected');
            $('#top_left option[value="' + responce.data.top_left + '"]').removeAttr('selected');
            $('#top_right option[value="' + responce.data.top_right + '"]').removeAttr('selected');
            $('#bottom_left option[value="' + responce.data.bottom_left + '"]').removeAttr('selected');
            $('#bottom_right option[value="' + responce.data.bottom_right + '"]').removeAttr('selected');
            // end reset form

            $('#card_post').attr('data-formid', responce.data.name);
            $('input[name="graph_color"]').removeAttr('checked'); // Remove first checked checkbox
            $('.g_clr').removeClass('active');

            $('#timeframe option[value="' + responce.data.timeframe + '"]').attr('selected', 'selected');
            $('#graph option[value="' + responce.data.graph + '"]').attr('selected', 'selected');
            $('#graph_type option[value="' + responce.data.graph_type + '"]').attr('selected', 'selected');
            $('#top_left option[value="' + responce.data.top_left + '"]').attr('selected', 'selected');
            $('#top_right option[value="' + responce.data.top_right + '"]').attr('selected', 'selected');
            $('#bottom_left option[value="' + responce.data.bottom_left + '"]').attr('selected', 'selected');
            $('#bottom_right option[value="' + responce.data.bottom_right + '"]').attr('selected', 'selected');
            $('input[name=graph_color][value="' + responce.data.graph_color + '"]').attr('checked', 'checked');
            $('input[name=graph_color][value="' + responce.data.graph_color + '"]').parent('.g_clr').addClass('active');

            $('select').selectric('refresh');
            $('.site_title').html(site);
            $("#site_card").modal("show");
        }
    });
}

function submit_cardData(site, formData) {
    var data = {
        'site': site,
        'formData': formData,
        'actionType': 'set_card_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            $("#site_card").modal("hide");
            $('#' + responce.data + '_wrapper').load('quick_view.php #rec_' + responce.data); //note: the space before #div1 is very important
            loadQuickView(site, responce.data);
        }
    })
}

function loadQuickView(site, id) {

    $("#chart_loader_" + id).removeClass('d-none');
    $('#chart-' + id).addClass('d-none');

    var data = {
        'site': site,
        'chartType': 'quick_view'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.data.is_success) {
            var txtData = responce.data.data;

            $.each(txtData.metrics, function (key, val) {
                var k = Object.keys(val)[0];
                var v = Object.values(val)[0];

                $('#' + key + '_id_' + txtData.site.id).html(k);
                $('#' + key + '_value_' + txtData.site.id).html(v);
            });
        }

        if (responce.chart.is_success) {
            $("#chart_loader_" + responce.data.data.site.id).addClass('d-none');
            $('#chart-' + responce.data.data.site.id).removeClass('d-none');
            var chartData = responce.chart.data;
            var siteData = responce.data.data.site;
            if ($('#chart-' + responce.data.data.site.id).length > 0) {
                drawQuickChart(chartData, siteData);
            }
        }
    });
}

function drawQuickChart(chartData, siteData) {
    var quick_chart = new Chart($('#chart-' + siteData.id), {
        type: siteData.graph_type,
        options: {
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }],
            },
        },
        data: {
            labels: chartData.labels,
            datasets: [{
                label: siteData.graph,
                backgroundColor: siteData.graph_color,
                borderColor: siteData.graph_color,
                fill: '!0',
                data: chartData.datasets
            }]
        }
    });
}

// end Quick View Page Data

// Analytics Page
function getAnalyticsData() {

    $(".chart_loader_canvas").removeClass('d-none');
    $('#chart-line').addClass('d-none');
    $('#chart-bar').addClass('d-none');

    var segment = '';
    var activeMetrics = 'sessions';
    var date = $('#date_duration').val();
    var sitename = $('#site').val();
    if ($('.analytics_data').hasClass('btn-primary')) {
        segment = $('.analytics_data.btn-primary').attr('data-value');
    }
    if ($('.card-metrics').hasClass('bg-primary')) {
        activeMetrics = $('.card-metrics.bg-primary').attr('id');
    }

    var data = {
        'site': sitename,
        'date': date,
        'segment': segment,
        'actionType': 'get_analytics_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {

            $(".chart_loader_canvas").addClass('d-none');
            $('#chart-line').removeClass('d-none');
            $('#chart-bar').removeClass('d-none');

            $.each(responce.data, function (key, val) {
                var v = parseFloat(val);
                if (!Number.isInteger(v)) {
                    v = v.toFixed(2);
                }
                $('#' + key + ' .ga_val').html(v);
            });
            arrChartRec = responce.chart;
            drawUsagesChart(activeMetrics);
        }
    });
}

// End Analytics page

// Audience Page
function getAudienceData() {

    $(".chart_loader_canvas").removeClass('d-none');
    $('#chart-line').addClass('d-none');
    $('#chart-bar').addClass('d-none');

    var dimension = '';
    var activeCard = 'sessions';
    var date = $('#date_duration').val();

    var sitename = $('#site').val();
    if ($('.audience_data').hasClass('btn-primary')) {
        dimension = $('.audience_data.btn-primary').attr('data-value');
    }
    if ($('.card-audience').hasClass('bg-primary')) {
        activeCard = $('.card-audience.bg-primary').attr('id');
    }

    var data = {
        'site': sitename,
        'date': date,
        'dimension': dimension,
        'actionType': 'get_audience_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);

        if (responce.is_success) {
            $(".chart_loader_canvas").addClass('d-none');
            $('#chart-line').removeClass('d-none');
            $('#chart-bar').removeClass('d-none');

            $.each(responce.data, function (key, val) {
                var v = parseFloat(val);
                if (!Number.isInteger(v)) {
                    v = v.toFixed(2);
                }
                $('#' + key + ' .ga_val').html(v);
            });
            arrChartRec = responce.chart;
            drawUsagesChart(activeCard);
        }
    });
}

// End Audience Page

// Pages Page
function getPagesData() {

    $(".chart_loader_canvas").removeClass('d-none');
    $('#chart-line').addClass('d-none');
    $('#chart-bar').addClass('d-none');

    var dimension = '';
    var activeCard = 'sessions';
    var date = $('#date_duration').val();

    var sitename = $('#site').val();
    if ($('.pages_data').hasClass('btn-primary')) {
        dimension = $('.pages_data.btn-primary').attr('data-value');
    }
    if ($('.card-pages').hasClass('bg-primary')) {
        activeCard = $('.card-pages.bg-primary').attr('id');
    }

    var data = {
        'site': sitename,
        'date': date,
        'dimension': dimension,
        'actionType': 'get_pages_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);

        if (responce.is_success) {
            $(".chart_loader_canvas").addClass('d-none');
            $('#chart-line').removeClass('d-none');
            $('#chart-bar').removeClass('d-none');

            $.each(responce.data, function (key, val) {
                var v = parseFloat(val);
                if (!Number.isInteger(v)) {
                    v = v.toFixed(2);
                }
                $('#' + key + ' .ga_val').html(v);
            });
            arrChartRec = responce.chart;
            drawUsagesChart(activeCard);
        }
    });
}

// End Pages Page

// SEO Page
function getSEOData() {

    $(".chart_loader_canvas").removeClass('d-none');
    $('#chart-line').addClass('d-none');
    $('#chart-bar').addClass('d-none');

    var dimension = '';
    var activeCard = 'sessions';
    var date = $('#date_duration').val();

    var sitename = $('#site').val();
    if ($('.seo_data').hasClass('btn-primary')) {
        dimension = $('.seo_data.btn-primary').attr('data-value');
    }
    if ($('.card-seo').hasClass('bg-primary')) {
        activeCard = $('.card-seo.bg-primary').attr('id');
    }

    var data = {
        'site': sitename,
        'date': date,
        'dimension': dimension,
        'actionType': 'get_seo_data'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);

        if (responce.is_success) {
            $(".chart_loader_canvas").addClass('d-none');
            $('#chart-line').removeClass('d-none');
            $('#chart-bar').removeClass('d-none');

            $.each(responce.data, function (key, val) {
                var v = parseFloat(val);
                if (!Number.isInteger(v)) {
                    v = v.toFixed(2);
                }
                $('#' + key + ' .ga_val').html(v);
            });
            arrChartRec = responce.chart;
            drawUsagesChart(activeCard);
        }
    });
}

// End SEO Page

// Draw Chart
function drawUsagesChart(metrics) {
    var labels;
    var dataVal;
    var pos = $(document).scrollTop();
    var met = arrChartRec[metrics];

    if (met != undefined && met != '') {
        labels = Object.keys(met);
        dataVal = Object.values(met);
    } else {
        labels = '';
        dataVal = 0;
    }

    var label = metrics.charAt(0).toUpperCase() + metrics.slice(1);

    // Destroy Chart Old Data and Fill new data
    if (chart1) {
        chart1.destroy();
    }
    if (chart2) {
        chart2.destroy();
    }

    // Line Chart
    if ($('#chart-line').length) {
        chart1 = new Chart($('#chart-line'), {
            type: 'line',
            options: {
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            autoSkip: true,
                            maxTicksLimit: 14
                        }
                    }],
                },
            },
            data: {
                labels: labels,
                datasets: [{
                    label: label,
                    backgroundColor: '#5e72e45e',
                    borderColor: '#5e72e4',
                    data: dataVal
                }]
            }
        });
    }

    // Bar Chart
    if ($('#chart-bar').length) {
        chart2 = new Chart($('#chart-bar'), {
            type: 'bar',
            options: {
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            autoSkip: true,
                            maxTicksLimit: 14
                        }
                    }],
                },
            },
            data: {
                labels: labels,
                datasets: [{
                    label: label,
                    backgroundColor: '#5e72e4',
                    data: dataVal,
                }]
            }
        });
    }

    // Prevent scroll after update both chart
    $(document).scrollTop(pos);
}

// End


// Make Button and Card Active
function makeButtonActive(current, currClass) {
    if ($('.' + currClass).hasClass('btn-primary')) {
        $('.' + currClass).removeClass('btn-primary');
        $('.' + currClass).addClass('btn-white');
    }
    current.toggleClass("btn-white").toggleClass("btn-primary");
}

function makeCardActive(current, currClass) {
    $('.' + currClass).find('.ga_val').removeClass('text-white');
    current.find('.ga_val').toggleClass("text-white");
    if ($('.' + currClass).hasClass('bg-primary')) {
        $('.' + currClass).removeClass('bg-primary');
        $('.' + currClass).addClass('bg-white');
    }
    current.toggleClass("bg-white").toggleClass("bg-primary");

    if (current.find('.text-sm').addClass('text-white')) {
        $('.' + currClass).find('.text-sm').removeClass('text-white');
        $('.' + currClass).find('.text-sm').addClass('text-gray');
    }
    current.find('.text-sm').toggleClass("text-gray").toggleClass("text-white");
}

// End make button and card active


// Custom Page and Chart Drawn Function

function get_session_by_device() {
    var data = {
        'site': $("#site").val(),
        'chartType': 'session_by_device'
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            if ($('#session_by_device').length) {
                var doughnutChart = new Chart($('#session_by_device'), {
                    type: 'doughnut',
                    data: {
                        labels: responce.data.label,
                        datasets: [{
                            data: responce.data.datasets,
                            backgroundColor: [
                                "#42a4f4",
                                "#F64A91",
                                "#7468bd"
                            ],
                            label: 'Dataset 1',
                        }],
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                    }
                });
            }
        }
    });
}

function get_active_pages() {
    var data = {
        'site': $("#site").val(),
        'chartType': 'active_pages'
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            var html = '';
            $.each(responce.data, function (i, item) {
                html += '<tr>';
                html += '<th scope="row">' + (i + 1) + '</th>';
                html += '<td>' + item.PageUrl + '</td>';
                html += '<td>' + item.Users + '</td>';
                html += '<td>' + item.NewSessions + '%</td>';
                html += '</tr>';
            });
            $("#active_pages").html(html);
        }
    });
}

function get_live_user() {
    var data = {
        'site': $("#site").val(),
        'chartType': 'live_user'
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            $("#live_users").html(responce.liveUser);
        }
    });
}

function get_world_map() {

    var data = {
        'site': $("#site").val(),
        'chartType': 'worldmap'
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            $('#world-map').vectorMap({
                map: 'world_mill_en',
                scaleColors: ['#F54086', '#695DB5'],
                normalizeFunction: 'polynomial',
                focusOn: {
                    x: 5,
                    y: 1,
                    scale: .85
                },
                zoomOnScroll: false,
                zoomMin: 0.65,
                hoverColor: false,
                regionStyle: {
                    initial: {
                        fill: '#c5d5ea',
                        "fill-opacity": 1,
                        stroke: '#c5d5ea',
                        "stroke-width": 0,
                        "stroke-opacity": 0
                    },
                    hover: {
                        "fill-opacity": 0.6
                    }
                },
                markerStyle: {
                    initial: {
                        fill: '#5e72e4',
                        stroke: '#b3ace5',
                        "fill-opacity": 1,
                        "stroke-width": 6,
                        "stroke-opacity": 0.8,
                        r: 3
                    },
                    hover: {
                        stroke: '#b3ace5',
                        "stroke-width": 10
                    },
                    selected: {
                        fill: 'blue'
                    },
                    selectedHover: {}
                },
                backgroundColor: '#ffffff',
                markers: responce.data
            });
        }
    });
}

function get_chart_custom() {
    $("#empty_custom_chart").hide();
    $("#container_custom_chart").html('<div class="text-center" id="chart_loader"><img src="assets/img/loader.gif"/></div><canvas id="chart_custom" style="display: none"></canvas>');
    var ctx = document.getElementById('chart_custom').getContext('2d');
    var data = {
        'site': $("#site").val(),
        'chartType': 'chart_custom',
        'chart_duration': $(".duration").val(),
        'metrics': $("#custom_form .metrics").val(),
        'dimension': $("#custom_form .dimension").val(),
    };

    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            $("#chart_loader").hide();
            $("#chart_custom").show();
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: responce.data,
            });
        } else {
            $("#chart_loader").hide();
            $("#empty_custom_chart").show();
            $("#empty_custom_chart").html('<h4 class="h4 text-danger">Metrics & Dimension Both Are Required.</h4>');
        }
    });
}

function get_chart_user_type() {
    var ctx = document.getElementById('chart_user_type').getContext('2d');
    var data = {
        'site': $("#site").val(),
        'chartType': 'chart_user_type',
        'chart_duration': $("#card_user_type .chart_duration .nav-item .active").html()
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        if (responce.is_success) {
            $(".total_visitor").html(responce['total']['New Visitor']);
            $(".total_returning_visitor").html(responce['total']['Returning Visitor']);
            // console.log(responce.data);
            var myChart = new Chart(ctx, {
                type: 'line',
                data: responce.data
            });
        }
    });
}

function get_usersChart() {
    var ctx = document.getElementById('chart_user_type').getContext('2d');
    var data = {
        'site': $("#site").val(),
        'chartType': 'usersChart',
        'chart_duration': 'Year'
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        // console.log(responce);

        if (responce.is_success) {
            $(".total_user").html(responce['total']['Users']);
            var ctx = document.getElementById("usersChart").getContext("2d");
            var gradient = ctx.createLinearGradient(0, 0, 0, 240);
            gradient.addColorStop(0, '#11cdef');
            gradient.addColorStop(1, Chart.helpers.color('#ffffff').alpha(0).rgbString());
            var config = {
                type: 'line',
                data: {
                    labels: responce['data']['labels'],
                    datasets: [{
                        label: responce['data']['datasets'][0]['label'],
                        backgroundColor: gradient,
                        borderWidth: 2,
                        borderColor: '#11cdef',
                        pointBackgroundColor: '#11cdef',
                        pointBorderColor: Chart.helpers.color('#ffffff').alpha(0).rgbString(),
                        pointHoverBackgroundColor: Chart.helpers.color('#ffffff').alpha(0.1).rgbString(),
                        pointHoverBorderColor: Chart.helpers.color('#ffffff').alpha(0.1).rgbString(),
                        data: responce['data']['datasets'][0]['data']
                    }]
                },
                options: {
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0.000001
                        },
                        point: {
                            radius: 4,
                            borderWidth: 8
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 50,
                            bottom: 0
                        }
                    }
                }
            };

            var chart = new Chart(ctx, config);
        }
    });
}

function get_bounceRateChart() {
    var ctx = document.getElementById('bounceRateChart').getContext('2d');
    var data = {
        'site': $("#site").val(),
        'chartType': 'bounceRateChart',
        'chart_duration': 'Year'
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        // console.log(responce);

        if (responce.is_success) {
            $(".total_bounce_rate").html(responce['total']['Bounce Rate']);
            var ctx = document.getElementById("bounceRateChart").getContext("2d");
            var gradient = ctx.createLinearGradient(0, 0, 0, 240);
            gradient.addColorStop(0, '#fb6340');
            gradient.addColorStop(1, Chart.helpers.color('#ffffff').alpha(0).rgbString());
            var config = {
                type: 'line',
                data: {
                    labels: responce['data']['labels'],
                    datasets: [{
                        label: responce['data']['datasets'][0]['label'],
                        backgroundColor: gradient,
                        borderWidth: 2,
                        borderColor: '#fb6340',
                        pointBackgroundColor: '#fb6340',
                        pointBorderColor: Chart.helpers.color('#ffffff').alpha(0).rgbString(),
                        pointHoverBackgroundColor: Chart.helpers.color('#ffffff').alpha(0.1).rgbString(),
                        pointHoverBorderColor: Chart.helpers.color('#ffffff').alpha(0.1).rgbString(),
                        data: responce['data']['datasets'][0]['data']
                    }]
                },
                options: {
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0.000001
                        },
                        point: {
                            radius: 4,
                            borderWidth: 8
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 50,
                            bottom: 0
                        }
                    }
                }
            };

            var chart = new Chart(ctx, config);
        }
    });
}

function get_sessionDuration() {
    var ctx = document.getElementById('sessionDuration').getContext('2d');
    var data = {
        'site': $("#site").val(),
        'chartType': 'sessionDuration',
        'chart_duration': 'Year'
    };
    $.post(url, data, function (responce) {
        responce = JSON.parse(responce);
        // console.log(responce);

        if (responce.is_success) {
            $(".total_session_duration").html(responce['total']['Session Duration']);
            var ctx = document.getElementById("sessionDuration").getContext("2d");

            var gradient = ctx.createLinearGradient(0, 0, 0, 240);
            gradient.addColorStop(0, "#5e72e4");
            gradient.addColorStop(1, Chart.helpers.color('#ffffff').alpha(0).rgbString());
            var config = {
                type: 'line',
                data: {
                    labels: responce['data']['labels'],
                    datasets: [{
                        label: responce['data']['datasets'][0]['label'],
                        backgroundColor: gradient,
                        borderWidth: 2,
                        borderColor: "#5e72e4",
                        pointBackgroundColor: "#5e72e4",
                        pointBorderColor: Chart.helpers.color('#ffffff').alpha(0).rgbString(),
                        pointHoverBackgroundColor: Chart.helpers.color('#ffffff').alpha(0.1).rgbString(),
                        pointHoverBorderColor: Chart.helpers.color('#ffffff').alpha(0.1).rgbString(),
                        data: responce['data']['datasets'][0]['data']
                    }]
                },
                options: {
                    title: {
                        display: false,
                    },
                    tooltips: {
                        mode: 'nearest',
                        intersect: false,
                        position: 'nearest',
                        xPadding: 10,
                        yPadding: 10,
                        caretPadding: 10
                    },
                    legend: {
                        display: false
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Month'
                            }
                        }],
                        yAxes: [{
                            display: false,
                            gridLines: false,
                            scaleLabel: {
                                display: true,
                                labelString: 'Value'
                            },
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    elements: {
                        line: {
                            tension: 0.000001
                        },
                        point: {
                            radius: 4,
                            borderWidth: 8
                        }
                    },
                    layout: {
                        padding: {
                            left: 0,
                            right: 0,
                            top: 50,
                            bottom: 0
                        }
                    }
                }
            };

            var chart = new Chart(ctx, config);
        }
    });
}

// End