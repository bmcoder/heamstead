<?php
require_once 'connection.php';
$db = new DB();
require_once 'admin_security.php';

if(isset($_SESSION['token']))
{
    unset($_SESSION['token']);
}


if(isset($_GET['delete']))
{
    $result        = $db->delete('site', ['id' => $_GET['delete']]);
    $remove_widget = $db->delete('widget', ['site_id' => $_GET['delete']]);
    header('location:site.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
</head>
<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Sites</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Sites</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="add_site.php" class="btn btn-neutral">Add New Site</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <h5 class="card-header">All Sites</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Site Name</th>
                                            <th>Account ID</th>
                                            <th>Property ID</th>
                                            <th>View ID</th>
                                            <th class="text-right" width="200px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $arrSite = $db->select('site');
                                        if($arrSite['total_record'])
                                        {
                                            while($row = $arrSite['rs']->fetch_object())
                                            {
                                                ?>
                                                  <?php if($curr_user->type == 1){ ?>
                                                <tr>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->account_id; ?></td>
                                                    <td><?php echo $row->property_id; ?></td>
                                                    <td><?php echo $row->project_id; ?></td>
                                                    <td class="text-right">
                                                        <a class="btn btn-primary btn-sm" href="update_site.php?id=<?php echo $row->id; ?>">Update</a>
                                                        <a onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm" href="site.php?delete=<?php echo $row->id; ?>">Delete</a>
                                                    </td>
                                                </tr>
                                    <?php }else{
                                             if($curr_user->username == $row->user)
                                                {
                                    ?>
                                     <tr>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->account_id; ?></td>
                                                    <td><?php echo $row->property_id; ?></td>
                                                    <td><?php echo $row->project_id; ?></td>
                                                    <td class="text-right">
                                                        <a class="btn btn-primary btn-sm" href="update_site.php?id=<?php echo $row->id; ?>">Update</a>
                                                        <a onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm" href="site.php?delete=<?php echo $row->id; ?>">Delete</a>
                                                    </td>
                                                </tr>
                                    
                                     <?php
                                        }
                                    }?>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->

    <?php require_once 'footer.php' ?>


</body>

</html>
