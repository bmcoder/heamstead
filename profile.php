<?php
require_once 'connection.php';
$ga = new GA();
$db = new DB();
require_once 'admin_security.php';

require_once __DIR__ . '/vendor/autoload.php';

$profile_pic_err = '';
$favicon_pic_err = '';
$logo_pic_err    = '';
$name_err        = '';

if(isset($_POST['update_profile']))
{
    $profile_pic = '';
    $login_user  = $_SESSION['user'];
    if(isset($_FILES['profile_pic']) && !empty($_FILES['profile_pic']['name']))
    {
        $profile_img = $db->fileUpload('profile_pic', 'assets/img/user_img/');
        if($profile_img['is_success'])
        {
            $usrInfo = $db->select('admin', ['username' => $login_user]);
            $arrRec  = $usrInfo['rs']->fetch_object();

            if(!empty($arrRec->profile_pic))
            {
                $old_file = __dir__ . '/assets/img/user_img/' . $arrRec->profile_pic;
                unlink($old_file);
            }

            $profile_pic = $profile_img['file_name'];
        }
        else
        {
            $profile_pic_err = $profile_img['error'];
        }
    }

    $arrData = [];
    if(isset($_POST['name']) && !empty($_POST['name']))
    {
        $arrData['name'] = $_POST['name'];
    }
    else
    {
        $name_err = 'Please Enter valid name.';
    }
    if(isset($_POST['password']) && !empty($_POST['password']))
    {
        $arrData['password'] = $_POST['password'];
    }
    if(isset($profile_pic) && !empty($profile_pic))
    {
        $arrData['profile_pic'] = $profile_pic;
    }

    if(!empty($arrData))
    {
        $update_img = $db->update('admin', $arrData, ['username' => $login_user]);
    }
}

if(isset($_POST['update_setting']))
{
    if(isset($_FILES['favicon']) && !empty($_FILES['favicon']['name']))
    {
        $favicon_img = $db->fileUpload('favicon', 'assets/img/logo/', ['png'], 'favicon');

        if(!$favicon_img['is_success'])
        {
            $favicon_pic_err = $favicon_img['error'];
        }
    }

    if(isset($_FILES['icon']) && !empty($_FILES['icon']['name']))
    {
        $icon_img = $db->fileUpload('icon', 'assets/img/logo/', ['png'], 'logo');

        if(!$icon_img['is_success'])
        {
            $logo_pic_err = $icon_img['error'];
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
</head>
<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Sites</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Edit Profile</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="nav-wrapper">
                            <ul class="nav nav-pills nav-fill flex-column flex-md-row" id="tabs-icons-text" role="tablist">
                                <li class="pr-2 pb-2">
                                    <a class="nav-link mb-sm-3 mb-md-0 active" id="tabs-icons-text-1-tab" data-toggle="tab" href="#tabs-icons-text-1" role="tab" aria-controls="tabs-icons-text-1" aria-selected="true">Update Profile</a>
                                </li>
                                <li class="pr-2 pb-2">
                                    <a class="nav-link mb-sm-3 mb-md-0" id="tabs-icons-text-2-tab" data-toggle="tab" href="#tabs-icons-text-2" role="tab" aria-controls="tabs-icons-text-2" aria-selected="false">Update Setting</a>
                                </li>
                            </ul>
                        </div>
                        <div class="card shadow rounded">
                            <div class="card-body">
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="tabs-icons-text-1" role="tabpanel" aria-labelledby="tabs-icons-text-1-tab">
                                        <form method="post" name="edit_form" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                                        <label for="edit_name">Name</label>
                                                        <input type="text" class="form-control" id="edit_name" name="name" value="<?php echo $curr_user->name; ?>" required/>
                                                        <?php
                                                        if(!empty($name_err))
                                                        { ?>
                                                            <span class="text-danger text-sm"><?php echo $name_err; ?></span><br>
                                                        <?php }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-lg-6">
                                                        <label for="edit_password">Password</label>
                                                        <input type="password" class="form-control" id="edit_password" name="password" minlength="8">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-12 pb-3 pb-md-0 pb-lg-0">
                                                        <label for="edit_profile_pic">Profile Pic</label>
                                                        <input type="file" class="form-control" id="edit_profile_pic" name="profile_pic"/>
                                                        <?php
                                                        if(!empty($profile_pic_err))
                                                        {
                                                            foreach($profile_pic_err as $val)
                                                            { ?>
                                                                <span class="text-danger text-sm"><?php echo $val; ?></span><br>
                                                            <?php }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-md-6 col-sm-12 text-center">
                                                        <?php
                                                        if(!empty($curr_user->profile_pic))
                                                        { ?>
                                                            <img src="assets/img/user_img/<?php echo $curr_user->profile_pic; ?>" class="icon_setting" onerror="this.onerror=null; this.src='assets/img/user_img/default.png'"/>
                                                            <?php
                                                        }
                                                        else
                                                        { ?>
                                                            <img src="assets/img/user_img/default.png" class="icon_setting"/>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button name="update_profile" class="btn btn-primary btn-rounded btn-floating" type="submit">Update Profile</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="tab-pane fade" id="tabs-icons-text-2" role="tabpanel" aria-labelledby="tabs-icons-text-2-tab">
                                        <form method="post" name="edit_setting" enctype="multipart/form-data">
                                            <div class="form-group">
                                                <div class="row pt-2">
                                                    <div class="col-6">
                                                        <label for="edit_profile_favicon">Change Favicon</label>
                                                        <input type="file" class="form-control" id="edit_profile_favicon" name="favicon">
                                                        <?php
                                                        if(!empty($favicon_pic_err))
                                                        {
                                                            foreach($favicon_pic_err as $val)
                                                            { ?>
                                                                <span class="text-danger text-sm"><?php echo $val; ?></span><br>
                                                            <?php }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-6 text-center">
                                                        <img src="assets/img/logo/favicon.png" class="icon_setting"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row pt-2">
                                                    <div class="col-6">
                                                        <label for="edit_profile_icon">Change Main Icon</label>
                                                        <input type="file" class="form-control" id="edit_profile_icon" name="icon">
                                                        <?php
                                                        if(!empty($logo_pic_err))
                                                        {
                                                            foreach($logo_pic_err as $val)
                                                            { ?>
                                                                <span class="text-danger text-sm"><?php echo $val; ?></span><br>
                                                            <?php }
                                                        }
                                                        ?>
                                                    </div>
                                                    <div class="col-6 text-center">
                                                        <img src="assets/img/logo/logo.png" class="icon_setting"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div>
                                                <button name="update_setting" class="btn btn-primary btn-rounded btn-floating" type="submit">Update Setting</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <?php require_once 'footer.php' ?>
    <?php
    if($update_img)
    {
        echo "<script>toaster('success','Profile Updated.');</script>";
    }
    if($favicon_img['is_success'] || $icon_img['is_success'])
    {
        echo "<script>toaster('success','Setting Updated.');</script>";
    }
    elseif(!empty($favicon_pic_err) || !empty($logo_pic_err))
    {
        echo "<script>toaster('error','Setting Not Updated.');</script>";
    }
    ?>
</body>
</html>