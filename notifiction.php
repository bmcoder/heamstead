<?php
require_once 'connection.php';
$db = new DB();
require_once 'admin_security.php';

if(isset($_SESSION['token']))
{
    unset($_SESSION['token']);
}


if(isset($_GET['delete']))
{
    $result        = $db->delete('site', ['id' => $_GET['delete']]);
    $remove_widget = $db->delete('widget', ['site_id' => $_GET['delete']]);
    header('location:site.php');
}


   

?>
<?php 
                            
       
        
        $servername = "localhost";
        $username = "hemstadn_bmcoder";
        $password = "eB]*P.7Tj~AT";
        $dbname = "hemstadn_visitor";
        
        $usercurrentr =$_SESSION['user'];
        // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
        // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }
        
        $sql = "UPDATE `notifications` SET `status` = '1' WHERE `notifications`.`username` = '$usercurrentr'";
        $result = $conn->query($sql);
        
       

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
</head>
<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Notifications</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Notifications</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <!--<a href="add_user.php" class="btn btn-neutral">Add New User</a>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            
                            <h5 class="card-header">All Notifications <?php echo $update; ?></h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Username</th>
                                            <th>Ip</th>
                                            <th>Country</th>
                                            <th>City</th>
                                            <th>Region</th>
                                             <th>Status</th>
                                             <th>Date</th>
                                            
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                         
                                        $notifications = $db->select('notifications', '', '*', 'id DESC');
                                        
                                        // $query = $db->select('site', '', '*', 'id ASC', '0,1');
                                  
                                        
                                        if($notifications['total_record'])
                                        {
                                            while($row = $notifications['rs']->fetch_object())
                                            {
                                                if($curr_user->username == $row->username){
                                                ?>
                                                
                                                <tr>
                                                    <td><?php echo $row->id; ?></td>
                                                    <td><?php echo $row->username; ?></td>
                                                    <td><?php echo $row->ip; ?></td>
                                                    <td><?php echo $row->country; ?></td>
                                                    <td><?php echo $row->city; ?></td>
                                                    <td><?php echo $row->region; ?></td>
                                                    <td><?php echo $row->status; ?></td>
                                                    <td><?php echo $row->date; ?></td>

                                                    
                                                </tr>
                                    
                                                <?php
                                                }
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->

    <?php require_once 'footer.php' ?>


</body>

</html>
