<?php
if(!isset($_SESSION['user']))
{
    header('location:index.php');
}

$siteName = '';

if(isset($_POST['update_site']))
{
    if(!empty($_POST['update_site']))
    {
        $query  = $db->select('site', ['name' => $_POST['update_site']], '*');
        $arrRec = $query['rs']->fetch_object();
    }

    if(isset($_SESSION['siteObj']))
    {
        unset($_SESSION['siteObj']);
    }

    if(isset($arrRec) && !empty($arrRec))
    {
        $_SESSION['siteObj'] = $arrRec;
    }
    else
    {
        $query               = $db->select('site', '', '*', 'id ASC', '0,1');
        $arrRec              = $query['rs']->fetch_object();
        $_SESSION['siteObj'] = $arrRec;
    }

    $siteName = $_SESSION['siteObj']->name;
}
elseif(!isset($_POST['site']))
{
    if(isset($_SESSION['siteObj']))
    {
        $siteName = $_SESSION['siteObj']->name;
    }
    elseif(empty($siteName))
    {
        $query = $db->select('site', '', '*', 'id ASC', '0,1');
        if($query['total_record'] > 0)
        {
            $arrRec              = $query['rs']->fetch_object();
            $_SESSION['siteObj'] = $arrRec;
            $siteName            = $_SESSION['siteObj']->name;
        }
    }
}

?>