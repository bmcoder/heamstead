<?php
require_once 'connection.php';
$ga = new GA();
$db = new DB();
require_once 'admin_security.php';

require_once __DIR__ . '/vendor/autoload.php';

if(isset($_GET['id']))
{
    $rec    = $db->select('site', ['id' => $_GET['id']]);
    $record = $rec['rs']->fetch_object();
}
else
{
    header('location:site.php');
}

if(isset($_POST['update_site']))
{
    $arrData               = [];
    $arrData['name']       = $_POST['name'];
    $arrData['color']      = $_POST['color'];
    $arrData['icon_class'] = $_POST['icon_class'];
    $update                = $db->update('site', $arrData, ['id' => $_GET['id']]);

    if($update)
    {
        header('location:site.php');
    }
    else
    {
        header('location:update_site.php?id=' . $_GET['id']);
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
</head>
<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Sites</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="site.php">Sites</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Update Site</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <h5 class="card-header">Update Site</h5>
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-group">
                                        <label for="demoTextInput1">Site Name</label>
                                        <input type="text" class="form-control" id="demoTextInput1" value="<?php echo $record->name ?>" required name="name" placeholder="Site name">
                                    </div>
                                    <?php
                                    $colors = [
                                        'primary',
                                        'success',
                                        'info',
                                        'danger',
                                        'warning',
                                    ];
                                    ?>
                                    <div class="form-group">
                                        <label for="color">Color</label>
                                        <select name="color" id="color" class="form-control" required>
                                            <?php
                                            foreach($colors as $value)
                                            {
                                                ?>
                                                <option value="<?php echo $value; ?>" <?php echo ($record->color == $value) ? 'selected' : '' ?> ><?php echo ucfirst($value); ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="icon_class">Site Icon Class</label>
                                        <input type="text" class="form-control" id="icon_class" name="icon_class" placeholder="fa fa-user" value="<?php echo $record->icon_class; ?>">
                                        <a href="https://fontawesome.com/v4.7.0/icons/" class="text-sm pt-1" target="_blank"><label>You can find icon class here.</label></a>
                                    </div>

                                    <div>
                                        <button name="update_site" class="btn btn-primary btn-rounded btn-floating" type="submit">Update Site</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <h5 class="card-header">All Sites</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Site Name</th>
                                            <th>Account ID</th>
                                            <th>Property ID</th>
                                            <th>View ID</th>
                                            <th class="text-right" width="200px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $arrSite = $db->select('site');
                                        if($arrSite['total_record'])
                                        {
                                            while($row = $arrSite['rs']->fetch_object())
                                            {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->account_id; ?></td>
                                                    <td><?php echo $row->property_id; ?></td>
                                                    <td><?php echo $row->project_id; ?></td>
                                                    <td class="text-right">
                                                        <a class="btn btn-primary btn-sm" href="update_site.php?id=<?php echo $row->id; ?>">Update</a>
                                                        <a onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm" href="add_site.php?delete=<?php echo $row->id; ?>">Delete</a>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->
    <?php require_once 'footer.php' ?>
</body>

</html>

