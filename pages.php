<?php
require_once 'connection.php';

$db = new DB();
$ga = new GA();

require_once 'admin_security.php';

if(isset($siteName))
{
    $site = $db->getAccessToken(['name' => $siteName]);

    if($site['isSuccess'])
    {
        $site = $site['objSite'];
    }
    else
    {
        $error = $site['message'];
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Pages - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
</head>

<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <?php
    if(isset($site))
    { ?>
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 d-inline-block mb-0">Pages Dashboard - <?php echo $site->name; ?></h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links">
                                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo $site->name; ?></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" value="<?php echo $site->name; ?>" id="site"/>
        <div class="content-wrapper">
            <div class="content container-fluid">
                <section class="page-content">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-10">
                                            <button class="pages_data btn btn-primary" data-value="ga:pageTitle">Page title</button>
                                            <button class="pages_data btn btn-white" data-value="ga:landingPagePath">Landing Page</button>
                                            <button class="pages_data btn btn-white" data-value="ga:exitPagePath">Exit Page</button>
                                        </div>
                                        <div class="col-2">
                                            <input type="text" name="date_duration" class="form-control date_duration" id="date_duration"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="chart-content">
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center chart_loader_canvas">
                                        <img src="assets/img/loader.gif"/>
                                    </div>
                                    <canvas id="chart-line" class="chart-canvas"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-center chart_loader_canvas">
                                        <img src="assets/img/loader.gif"/>
                                    </div>
                                    <canvas id="chart-bar" class="chart-canvas"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card card-pages bg-primary border-0" id="sessions">
                                <div class="card-body px-lg-5">
                                    <h1 class="text-white ga_val">0</h1>
                                    <b class="text-white text-sm">Sessions</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="users">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">Users</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="newUsers">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">New Users</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="pageviews">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">Pageviews</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="pageviewsPerSession">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">Pages/Session</b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="entranceRate">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">Entrances/Pageviews</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="percentNewSessions">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">% New Sessions</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="exitRate">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">% Exit</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="bounceRate">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">Bounce Rate</b>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="card card-pages bg-white border-0" id="goalCompletionsAll">
                                <div class="card-body px-lg-5">
                                    <h1 class="ga_val">0</h1>
                                    <b class="text-gray text-sm">Goal Complations</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 d-inline-block mb-0">Pages Dashboard</h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links">
                                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="content container-fluid">
                <section class="page-content">
                    <div class="col-12">
                        <div class="">
                            <div class="row">
                                <?php
                                $arrSite = $db->select('site');
                                if($arrSite['total_record'])
                                {
                                    while($row = $arrSite['rs']->fetch_object())
                                    { ?>
                                        <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-<?php echo $row->color ?>">
                                            <a href="dashboard.php?site=<?php echo $row->name; ?>">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span class="h2 font-weight-bold mb-0 text-white"><?php echo $row->name; ?></span>
                                                            <h5 class="card-title text-uppercase text-muted mb-0 text-white"><?php echo $row->project_id; ?></h5>
                                                        </div>
                                                        <div class="col-auto">
                                                            <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                                <?php if(!empty($row->icon_class)) { ?>
                                                                    <i class="<?php echo $row->icon_class; ?>"></i>
                                                                <?php } else { ?>
                                                                    <i class="ni ni-ungroup"></i>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-info">
                                    <a href="add_site.php">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <span class="h2 font-weight-bold mb-0 text-white">Add New Site</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                        <i class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<!-- END CONTENT WRAPPER -->


<script>
    var url = "<?php echo APP_URL . 'getChart.php' ?>";
</script>
<?php require_once 'footer.php' ?>

<script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="assets/js/chartjs-init.js"></script>

<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script>
    $(document).ready(function () {
        $('input[name = "date_duration"]').daterangepicker({
            opens: 'left',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            dateLimit: {
                'months': 2,
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
        });
    });
</script>

</body>
</html>
