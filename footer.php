<!-- Core -->
<script src="assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/js-cookie/js.cookie.js"></script>
<script src="assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js"></script>
<script src="assets/vendor/jquery-selectric/jquery.selectric.min.js"></script>
<script src="assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js"></script>
<script src="assets/vendor/sweetalert2/dist/sweetalert2.all.min.js"></script>

<!-- Argon JS -->
<script src="assets/js/argon.js?v=1.1.0"></script>
<script type="text/javascript">
    function toaster(type, msg) {
        switch (type) {
            case 'basic':
                swal({
                    title: "Here's a message!",
                    text: msg,
                    buttonsStyling: false,
                    confirmButtonClass: 'd-none',
                    timer: 2000
                })
                break;
            case 'info':
                swal({
                    title: 'Info',
                    text: msg,
                    type: 'info',
                    buttonsStyling: false,
                    confirmButtonClass: 'd-none',
                    timer: 2000
                })
                break;
            case 'success':
                swal({
                    title: 'Success',
                    text: msg,
                    type: 'success',
                    buttonsStyling: false,
                    confirmButtonClass: 'd-none',
                    timer: 2000
                })
                break;
            case 'error':
                swal({
                    title: 'Error',
                    text: msg,
                    type: 'error',
                    buttonsStyling: false,
                    confirmButtonClass: 'd-none',
                    timer: 2000
                })
                break;
            case 'warning':
                swal({
                    title: 'Warning',
                    text: msg,
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: 'd-none',
                    timer: 2000
                })
                break;

            case 'question':
                swal({
                    title: 'Are you sure?',
                    text: msg,
                    type: 'question',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-default'
                })
                break;

            case 'confirm':
                swal({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-danger',
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonClass: 'btn btn-secondary'
                }).then((result) => {
                    if (result.value) {
                        // Show confirmation
                        swal({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonClass: 'btn btn-primary'
                        });
                    }
                })
                break;

            case 'image':
                swal({
                    title: 'Sweet',
                    text: "Modal with a custom image ...",
                    imageUrl: '../../assets/img/ill/ill-1.svg',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-primary',
                    confirmButtonText: 'Super!'
                });
                break;

            case 'timer':
                swal({
                    title: 'Auto close alert!',
                    text: 'I will close in 2 seconds.',
                    timer: 2000,
                    showConfirmButton: false
                });
                break;
        }
    }
</script>


