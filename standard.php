<?php
require_once 'connection.php';

$db = new DB();
$ga = new GA();

require_once 'admin_security.php';

if(isset($siteName))
{
    $site = $db->getAccessToken(['name' => $siteName]);
    if($site['isSuccess'])
    {
        $site = $site['objSite'];
    }
    else
    {
        $error = $site['message'];
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="assets/vendor/jvectormap-next/jquery-jvectormap.css">
</head>

<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Analytics Dashboard - <?php echo $site->name; ?></h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page"><?php echo $site->name; ?></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" value="<?php echo $site->name; ?>" id="site"/>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col">
                        <div class="card" id="card_user_type">
                            <div class="card-header bg-transparent">
                                <div class="row align-items-center">
                                    <div class="col">
                                        <h5 class="h3 mb-0">Visitors1</h5>
                                    </div>
                                    <div class="col text-right">
                                        <ul class="nav nav-pills nav-pills-primary justify-content-end chart_duration" id="pills-demo-1" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true">Week</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">Month</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false">Year</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body p-0">
                                <div class="row m-0 col-border-xl">
                                    <div class="col-md-2 p-20">
                                        <div class="card-body">
                                            <div class="card-profile-stats justify-content-center">
                                                <div class="mr-0">
                                                    <span class="heading text-primary total_visitor">0</span>
                                                    <span class="description">Total New Visitors</span>
                                                </div>
                                                <div>
                                                    <span class="heading text-danger total_returning_visitor">0</span>
                                                    <span class="description">Total Returning Visitor</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-10">
                                        <div class="card-body">
                                            <canvas id="chart_user_type"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="card-deck m-b-30">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="h3 mb-0">Users</h5>
                                </div>
                                <div class="card-body p-0">
                                    <h4 class="card-title text-info pt-2 pl-2 total_user">0</h4>
                                    <div class="h-200">
                                        <canvas id="usersChart"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="h3 mb-0">Bounce Rate</h5>
                                </div>
                                <div class="card-body p-0">
                                    <h4 class="card-title text-warning pt-2 pl-2 total_bounce_rate">0%</h4>
                                    <div class="h-200">
                                        <canvas id="bounceRateChart"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="h3 mb-0">Session Duration</h5>
                                </div>
                                <div class="card-body p-0">
                                    <h4 class="card-title text-primary pt-2 pl-2 total_session_duration">0</h4>
                                    <div class="h-200">
                                        <canvas id="sessionDuration"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row m-b-30">
                    <div class="col-9">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="h3 mb-0">New Users by Location</h5>
                            </div>
                            <div class="card-body">
                                <div id="world-map" style="height: 300px"></div>
                            </div>
                        </div>
                    </div>
                 <!--    <div class="col-3">
                        <div class="card bg-gradient-primary text-center">
                            <div class="card-header bg-transparent">
                                <h5 class="h3 mb-0 text-white">Live Active Users</h5>
                            </div>
                            <div class="card-body">
                                <div class="display-2 text-white">
                                    <i class="ni ni-chart-bar-32"></i>
                                </div>
                                <div class="display-4 text-white" id="live_users">0</div>
                                <span class=" text-white">Active Users</span>
                            </div>
                        </div>
                    </div> -->
                </div>
              <!--   <div class="row m-b-30">
                    <div class="col">
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="h3 mb-0">Top Active Pages</h5>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-hover">
                                            <thead class="thead-light">
                                            <tr>
                                                <th scope="col"></th>
                                                <th scope="col">Active Page</th>
                                                <th scope="col">Active Users</th>
                                                <th scope="col">% New Sessions</th>
                                            </tr>
                                            </thead>
                                            <tbody id="active_pages"></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="h3 mb-0">Sessions by device</h5>
                                </div>
                                <div class="card-body">
                                    <div class="card-body p-10">
                                        <canvas id="session_by_device" style="height:400px"></canvas>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </section>
        </div>
    </div>
</div>
<!-- END CONTENT WRAPPER -->

<script>
    var url = "<?php echo APP_URL . 'getChart.php' ?>";
</script>
<?php require_once 'footer.php' ?>

<script src="assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
<script src="assets/vendor/jvectormap-next/jquery-jvectormap-world-mill.js"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<!--<script src="assets/vendor/chart.js/dist/Chart.bundle.min.js"></script>-->
<script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="assets/js/chartjs-init.js"></script>

<!-- ================== DATE SCRIPTS ==================-->
<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script>
    $(document).ready(function () {
        $('input[name="dates"]').daterangepicker({
            opens: 'left',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    $(document).on("change", ".metrics", function () {
        var dimension = ($(this).find(':selected').data('dimension'));
        $(".dimension").html('');
        var html = '<option value="">Dimension</option>';
        $.each(dimension, function (k, v) {
            html += '<option value="' + k + '">' + v + '</option>';
        });
        $(".dimension").html(html);
    });

</script>

</body>

</html>
