<?php
require_once 'connection.php';
$db = new DB();
$ga = new GA();
require_once 'admin_security.php';

if(isset($_GET['site']))
{
    $query  = $db->select('site', ['name' => $_GET['site']], '*', 'id ASC', '0,1');
    $arrRec = $query['rs']->fetch_object();
    
  

    if(isset($arrRec) && !empty($arrRec) && isset($_SESSION['siteObj']))
    {
        unset($_SESSION['siteObj']);
    }

    if(isset($arrRec) && !empty($arrRec))
    {
        $_SESSION['siteObj'] = $arrRec;
    }
    else
    {
        $query               = $db->select('site', '', '*', 'id ASC', '0,1');
        $arrRec              = $query['rs']->fetch_object();
        $_SESSION['siteObj'] = $query;
    }

    header('location:standard.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
    <link rel="stylesheet" href="assets/vendor/jvectormap-next/jquery-jvectormap.css">
</head>

<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Analytics Dashboard</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="col-12">
                    <div class="">
                        <div class="row">
                            <?php
                            $arrSite = $db->select('site');
                            if($arrSite['total_record'])
                            {
                               
                                while($row = $arrSite['rs']->fetch_object())
                                 
                                { ?>
                                <?php if($curr_user->type == 1){ ?>
                                    <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-<?php echo $row->color ?>">
                                        <a href="dashboard.php?site=<?php echo $row->name; ?>">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col">
                                                        <span class="h2 font-weight-bold mb-0 text-white"><?php echo $row->name; ?></span>
                                                        <h5 class="card-title text-uppercase text-muted mb-0 text-white"><?php echo $row->project_id; ?></h5>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                            <?php if(!empty($row->icon_class)) { ?>
                                                                <i class="<?php echo $row->icon_class; ?>"></i>
                                                            <?php } else { ?>
                                                                <i class="ni ni-ungroup"></i>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <?php }else{
                                       
                                  
                                    if($curr_user->username == $row->user)
                                        {
                                    ?>
                                    
                                    
                                     <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-<?php echo $row->color ?>">
                                        <a href="dashboard.php?site=<?php echo $row->name; ?>">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col">
                                                        <span class="h2 font-weight-bold mb-0 text-white"><?php echo $row->name; ?></span>
                                                        <h5 class="card-title text-uppercase text-muted mb-0 text-white"><?php echo $row->project_id; ?></h5>
                                                    </div>
                                                    <div class="col-auto">
                                                        <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                            <?php if(!empty($row->icon_class)) { ?>
                                                                <i class="<?php echo $row->icon_class; ?>"></i>
                                                            <?php } else { ?>
                                                                <i class="ni ni-ungroup"></i>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    
                                    
                                    <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-info">
                                <a href="add_site.php">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col">
                                                <span class="h2 font-weight-bold mb-0 text-white">Add New Site</span>
                                            </div>
                                            <div class="col-auto">
                                                <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                    <i class="fa fa-plus"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

<!-- END CONTENT WRAPPER -->

<?php require_once 'footer.php' ?>

<script src="assets/vendor/jvectormap-next/jquery-jvectormap.min.js"></script>
<script src="assets/vendor/jvectormap-next/jquery-jvectormap-world-mill.js"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<!--<script src="assets/vendor/chart.js/dist/Chart.bundle.min.js"></script>-->
<script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="assets/js/chartjs-init.js"></script>

<!-- ================== DATE SCRIPTS ==================-->
<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>
</body>

</html>
