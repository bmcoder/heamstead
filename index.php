<?php

require_once 'connection.php';
if(!file_exists("./config/database.php") || !file_exists("./config/client_secret_native.json"))
{
    header('location:install.php');
}
if(isset($_SESSION['user']))
{
    header('location:standard.php');
}
if(isset($_POST['login']))
{
    $db                = new DB();
    $login             = [];
    $login['username'] = $_POST['username'];
    $login['password'] = $_POST['password'];
    $loginData         = $db->select('admin', $login);
    $err               = 0;

    if($loginData['total_record'] == 1)
    {
       
        $_SESSION['user'] = $_POST['username'];
        
        header("location:standard.php");
    }
    else
    {
        $err = 1;
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Live Chat and Reporting | Hemstad Reporting</title>
    <?php require_once 'head.php' ?>
</head>

<body class="bg-default">

<div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-6 py-lg-6 pt-lg-9">
        <div class="container">
            <div class="header-body text-center mb-7">
                <div class="row justify-content-center">
                    <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                        <h1 class="text-white">Hemstad Reporting</h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
            </svg>
        </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
        <div class="row justify-content-center">
            <div class="col-lg-5 col-md-7">
                <div class="card bg-secondary border-0 mb-0">
                    <div class="card-header bg-transparent">
                        <div class="text-muted text-center mt-2 mb-3">
                            <img src="assets/img/logo/logo.png" alt="Rajodiya Infotech Logo" class="icon_setting"/>
                        </div>
                    </div>
                    <div class="card-body px-lg-5 py-lg-5">
                        <div class="text-center text-muted mb-4">
                            <small>Sign in with credentials</small>
                        </div>
                        <form role="form" method="post">
                            <div class="form-group mb-3">
                                <div class="input-group input-group-merge input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                                    </div>
                                    <input type="email" name="username" id="inputEmail" class="form-control" placeholder="Email address" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-merge input-group-alternative">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                    </div>
                                    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
                                </div>
                            </div>
                            <?php if(isset($err) && $err == 1) { ?>
                                <div class="form-group">
                                    <span class="text text-danger">Invalid Login</span>
                                </div>
                            <?php } ?>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary my-4" name="login">Sign in</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="py-5" id="footer-main">
    <div class="container">
        <div class="row align-items-center justify-content-xl-between">
            <div class="col-xl-12">
                <div class="copyright text-center text-muted">
                    Copyright &copy; <?php echo date('Y') ?> <a href="https://rajodiya.com" class="font-weight-bold ml-1" target="_blank">Hemstad</a>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php require_once 'footer.php'; ?>
</body>

</html>
