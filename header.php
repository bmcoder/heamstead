<?php
$curr_user = $db->select('admin', ['username' => $_SESSION['user']])['rs']->fetch_object();
$arrSite   = $db->select('site');
?>
<nav class="navbar navbar-top navbar-expand navbar-light bg-secondary border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse">
            <ul class="navbar-nav align-items-center ml-md-auto d-xl-none">
                <li class="nav-item d-xl-none">
                    <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin" data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="row w-75">
                <?php

                $arrNonHeader = [
                    'quick_view.php',
                    'dashboard.php',
                    'site.php',
                    'profile.php',
                    'update_site.php',
                ];

                if(!in_array(basename($_SERVER['PHP_SELF']), $arrNonHeader))
                { ?>
                    <div class="col-sm-12 col-md-3 col-md-3">
                        <form method="POST" class="mr-sm-3 w-100" id="submit_site">
                            <div class="form-group mb-0">
                                <div class="input-group">
                                    
                                   
                                    <select name="update_site" id="update_site" class="form-control w-75 selectric" onchange="this.form.submit()">
                                        <option value="">Please Select Site</option>
                                        <?php
                                         $i=0;
                                        if($arrSite['total_record'])
                                        {
                                            while($row = $arrSite['rs']->fetch_object())
                                            { ?>
                                            <?php if($curr_user->type == 1){ ?>
                                                <option value="<?php echo $row->name; ?>" <?php echo ($_SESSION['siteObj']->name == $row->name) ? 'selected' : ''; ?>><?php echo $row->name; ?></option>
                                               
                                                <?php }else{
                                         if($curr_user->username == $row->user)
                                        {
                                            $i++;
                                    ?>
                                    <option value="<?php echo $row->name; ?>" <?php echo ($i == 1) ? 'selected' : ''; ?>><?php echo $row->name;  ?></option>
                                    
                                      <?php
                                        }
                                    }?>
                                                <?php
                                            }
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php } ?>
            </div>
                    <style>
                    .notifiction12  {
                       display: block;
                    justify-content: center;
                    align-items: center;
                    vertical-align: middle;
                    font-weight: 900;
                    height: 30px;
                    width: 30px;
                    background: red;
                    width: red !important;
                    border-radius: 15px;
                    text-align: center;
                }   
        
        
        .notifiction12 i {
            color: white !important;
        }
        div#notifiction_wrap {
        /*position: absolute;*/
        /* bottom: 0; */
        background: white;
        color: #F44336;
        padding: 5px;
        }
        </style>
                <?php
                    
                    $notifications = $db->select('notifications',['username'=>$curr_user->username,'status'=>0]);
                    
                ?>
                
                
                  <div class="notification-bell">
                    <a href="notifiction.php" title="">
                        <img src="/assets/img/notification-icon.png" alt="">
                        <span><?php echo $notifications['total_record']; ?></span>
                    </a>
                  </div>  
              
            <ul class="navbar-nav align-items-center ml-auto ml-md-auto">
                <li class="nav-item dropdown">
                    
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                          <span class="avatar avatar-sm rounded-circle">
                              <?php if(!empty($curr_user->profile_pic)) { ?>
                                  <img alt="Image placeholder" class="avatar avatar-sm" src="assets/img/user_img/<?php echo $curr_user->profile_pic; ?>" onerror="this.onerror=null; this.src='assets/img/user_img/default.png'">
                              <?php } else { ?>
                                  <img alt="Image placeholder" class="avatar avatar-sm" src="assets/img/user_img/default.png">
                              <?php } ?>
                          </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm  font-weight-bold"><?php echo $curr_user->name; ?></span>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class="dropdown-header noti-title">
                            <h6 class="text-overflow m-0">Welcome!</h6>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a href="profile.php" class="dropdown-item">
                            <i class="ni ni-single-02"></i>
                            <span>Profile</span>
                        </a>
                        <a href="logout.php" class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>

