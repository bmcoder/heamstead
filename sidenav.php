<?php
$open = false;
if(basename($_SERVER['PHP_SELF']) == 'analytics.php' || basename($_SERVER['PHP_SELF']) == 'audience.php' || basename($_SERVER['PHP_SELF']) == 'pages.php' || basename($_SERVER['PHP_SELF']) == 'seo.php')
{
    $open = true;
}
require_once 'admin_security.php';
?>
<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <div class="sidenav-header d-flex align-items-center">
            <a class="navbar-brand" href="index.php">
                <img src="assets/img/logo/logo.png" class="navbar-brand-img" alt="...">
            </a>
            <div class="ml-auto">
                <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                    <div class="sidenav-toggler-inner">
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                        <i class="sidenav-toggler-line"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="navbar-inner">
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                <ul class="navbar-nav">
                    <?php if($_SESSION['user'] == 'admin@admin.com' ){ ?>
                     <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'user_index.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="user_index.php">
                            <i class="fas fa-user text-primary"></i>
                            <span class="nav-link-text">Users </span>
                        </a>
                    </li>
                    <?php } ?>

                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'standard.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="standard.php">
                            <i class="ni ni-diamond text-primary"></i>
                            <span class="nav-link-text">Dashboard</span>
                        </a>
                    </li>
                  
                    
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'quick_view.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="quick_view.php">
                            <i class="ni ni-active-40 text-info"></i>
                            <span class="nav-link-text">Quick View</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'widget.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="widget.php">
                            <i class="ni ni-archive-2 text-green"></i>
                            <span class="nav-link-text">Widget</span>
                        </a>
                    </li>
                    
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($open) ? "active" : ""; ?>" href="#navbar-analytics" data-toggle="collapse" role="button" aria-expanded="<?php echo ($open) ? "true" : "false"; ?>" aria-controls="navbar-analytics">
                            <i class="ni ni-chart-pie-35 text-danger"></i>
                            <span class="nav-link-text">Analytics</span>
                        </a>
                        <div class="collapse <?php echo ($open) ? "show" : ""; ?>" id="navbar-analytics">
                            <ul class="nav nav-sm flex-column">
                                <li class="nav-item">
                                    <a href="analytics.php" class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'analytics.php' && empty($_GET['site'])) ? 'active' : '' ?>">Channel</a>
                                </li>
                                <li class="nav-item">
                                    <a href="audience.php" class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'audience.php' && empty($_GET['site'])) ? 'active' : '' ?>">Audience</a>
                                </li>
                                <li class="nav-item">
                                    <a href="pages.php" class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'pages.php' && empty($_GET['site'])) ? 'active' : '' ?>">Pages</a>
                                </li>
                                <li class="nav-item">
                                    <a href="seo.php" class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'seo.php' && empty($_GET['site'])) ? 'active' : '' ?>">SEO</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'custom_dashboard.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="custom_dashboard.php">
                            <i class="ni ni-calendar-grid-58 text-primary"></i>
                            <span class="nav-link-text">Custom</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'site.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="site.php">
                            <i class="ni ni-ungroup text-orange"></i>
                            <span class="nav-link-text">Manage Sites</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link <?php echo (basename($_SERVER['PHP_SELF']) == 'dashboard.php' && empty($_GET['site'])) ? 'active' : '' ?>" href="dashboard.php">
                            <i class="ni ni-shop text-primary"></i>
                            <span class="nav-link-text">My Accounts</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>