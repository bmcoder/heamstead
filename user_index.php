<?php
require_once 'connection.php';
$db = new DB();
require_once 'admin_security.php';

if(isset($_SESSION['token']))
{
    unset($_SESSION['token']);
}


if(isset($_GET['delete']))
{
    $result        = $db->delete('site', ['id' => $_GET['delete']]);
    $remove_widget = $db->delete('widget', ['site_id' => $_GET['delete']]);
    header('location:site.php');
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
</head>
<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Users</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Users</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-lg-6 col-5 text-right">
                        <a href="add_user.php" class="btn btn-neutral">Add New User</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <h5 class="card-header">All Sites</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>username</th>
                                           
                                            <th>Password</th>
                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                         
                                        $admin = $db->select('admin');
                                       
                                        
                                        if($admin['total_record'])
                                        {
                                            while($row = $admin['rs']->fetch_object())
                                            {
                                                ?>
                                                <tr>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->username; ?></td>
                                                    <td><?php echo $row->password; ?></td>
                                                </tr>
                                    
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->

    <?php require_once 'footer.php' ?>


</body>

</html>
