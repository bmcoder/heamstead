<?php
require_once 'connection.php';
$ga = new GA();
$db = new DB();
require_once 'admin_security.php';

require_once __DIR__ . '/vendor/autoload.php';


if(isset($_GET['delete']))
{
    $result = $db->delete('site', ['id' => $_GET['delete']]);
    header('location:add_site.php');
}
if(isset($_POST['add_site']))
{


    $arrData                = [];
    $arrData['name']        = $_POST['name'];
    $arrData['project_id']  = $_POST['project_id'];
    $arrData['account_id']  = $_POST['account'];
    $arrData['property_id'] = $_POST['property'];
    $arrData['color']       = $_POST['color'];
    $arrData['icon_class']  = $_POST['icon_class'];
    $arrData['user']  = $_SESSION['user'];
    $arrResult              = $db->getAccessToken($arrData, true);

    if($arrResult['is_success'])
    {
        $_SESSION['success'] = "Site added.";
    }
    else
    {
        $_SESSION['error'] = $arrResult['message'];
    }
    header('location:site.php');
}

if(isset($_SESSION['token']) && $_SESSION['token'])
{
    $profiles = $ga->getProfiles();
    if(isset($_POST['for']) && $_POST['for'] == 'property')
    {
        if(!empty($_POST['id']))
        {
            $property = $ga->getProperty($_POST['id']);
            print_r(json_encode($property));
            die();
        }
    }

    if(isset($_POST['for']) && $_POST['for'] == 'view')
    {
        if(!empty($_POST['id']) && !empty($_POST['acc_id']))
        {
            $view = $ga->getView($_POST['acc_id'], $_POST['id']);
            print_r(json_encode($view));
            die;
        }
    }
}

if(!isset($_GET['code']) && empty($_SESSION['token']))
{
    $auth_url = $ga->getAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
    die;
}
else
{
    if(!empty($_GET['code']))
    {
        $_SESSION['token'] = $ga->getAccessToken($_GET['code']);
        header('Location: ' . filter_var(APP_URL . 'add_site.php', FILTER_SANITIZE_URL));
    }
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Hemstad</title>
    <?php require_once 'head.php'; ?>
</head>
<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <div class="header">
        <div class="container-fluid">
            <div class="header-body">
                <div class="row align-items-center py-4">
                    <div class="col-lg-6 col-7">
                        <h6 class="h2 d-inline-block mb-0">Sites</h6>
                        <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                            <ol class="breadcrumb breadcrumb-links">
                                <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                <li class="breadcrumb-item"><a href="site.php">Sites</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Add New Site</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-wrapper">
        <div class="content container-fluid">
            <section class="page-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <h5 class="card-header">Add Site</h5>
                            <div class="card-body">
                                <?php
                                if(isset($success))
                                {
                                    ?>
                                    <div class="alert alert-success"><?php echo $success; ?></div>
                                    <?php
                                }
                                ?>
                                <?php
                                if(isset($error))
                                {
                                    ?>
                                    <div class="alert alert-error"><?php echo $error; ?></div>
                                    <?php
                                }
                                ?>
                                <form method="post">
                                    <?php
                                    if($profiles['is_success'] == true && !empty($profiles['data']))
                                    { ?>
                                        <div class="form-group">
                                            <label for="demoTextInput1">Site Name</label>
                                            <input type="text" class="form-control" id="demoTextInput1" required name="name" placeholder="Site name">
                                        </div>
                                        <div class="form-group">
                                            <label for="account">Account</label>
                                            <select name="account" id="account" class="form-control" required>
                                                <?php foreach($profiles['data'] as $profile)
                                                { ?>
                                                    <option value="<?php echo $profile['id']; ?>"><?php echo $profile['name']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="property">Property</label>
                                            <select name="property" id="property" class="form-control" required></select>
                                        </div>

                                        <div class="form-group">
                                            <label for="view">View</label>
                                            <select name="project_id" id="view" class="form-control" required></select>
                                        </div>
                                        <?php
                                        $colors = [
                                            'primary',
                                            'success',
                                            'info',
                                            'danger',
                                            'warning',
                                        ];
                                        ?>
                                        <div class="form-group">
                                            <label for="color">Color</label>
                                            <select name="color" id="color" class="form-control" required>
                                                <?php
                                                foreach($colors as $value)
                                                {
                                                    ?>
                                                    <option value="<?php echo $value; ?>"><?php echo ucfirst($value); ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="icon_class">Site Icon Class</label>
                                            <input type="text" class="form-control" id="icon_class" name="icon_class" placeholder="fa fa-user">
                                            <a href="https://fontawesome.com/v4.7.0/icons/" class="text-sm pt-1" target="_blank"><label>You can find icon class here.</label></a>
                                        </div>

                                        <div>
                                            <button name="add_site" class="btn btn-primary btn-rounded btn-floating" type="submit">Add Site</button>
                                        </div>
                                        <?php
                                    }
                                    else
                                    {
                                        ?>
                                        <div class="alert alert-error"><?php echo $profiles['data']; ?></div>
                                        <?php
                                    }
                                    ?>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <h5 class="card-header">All Sites</h5>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Site Name</th>
                                            <th>Account ID</th>
                                            <th>Property ID</th>
                                            <th>View ID</th>
                                            <th class="text-right" width="200px">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $arrSite = $db->select('site');
                                        if($arrSite['total_record'])
                                        {
                                            while($row = $arrSite['rs']->fetch_object())
                                            {
                                                
                                                ?>
                                                   <?php if($curr_user->type == 1){ ?>
                                                <tr>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->account_id; ?></td>
                                                    <td><?php echo $row->property_id; ?></td>
                                                    <td><?php echo $row->project_id; ?></td>
                                                    <td class="text-right">
                                                        <a onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm" href="add_site.php?delete=<?php echo $row->id; ?>">Delete</a>
                                                    </td>
                                                </tr>
                                                 <?php }else{
                                       
                                  
                                    if($curr_user->username == $row->user)
                                        {
                                    ?>
                                    




                                              <tr>
                                                    <td><?php echo $row->name; ?></td>
                                                    <td><?php echo $row->account_id; ?></td>
                                                    <td><?php echo $row->property_id; ?></td>
                                                    <td><?php echo $row->project_id; ?></td>
                                                    <td class="text-right">
                                                        <a onclick="return confirm('Are you sure?');" class="btn btn-danger btn-sm" href="add_site.php?delete=<?php echo $row->id; ?>">Delete</a>
                                                    </td>
                                                </tr>



                                      <?php
                                        }
                                    }?>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- END CONTENT WRAPPER -->

    <?php require_once 'footer.php' ?>
    <script>
        $(document).ready(function () {
            $('#property').append('<option value="">Loading ...</option>');
            $('#view').append('<option value="">Loading ...</option>');
            var accId = $('#account').val();
            if (accId != '') {
                $.ajax({
                    type: 'POST',
                    data: {
                        for: 'property',
                        id: accId
                    },
                    success: function (data) {
                        $('#property').empty();
                        var propertys = JSON.parse(data);
                        var p_id = propertys['0']['id'];
                        $(propertys).each(function (key, value) {
                            $('#property').append('<option value="' + value.id + '" data-acc-id="' + value.acc_id + '">' + value.name + '</option>');
                        });

                        $.ajax({
                            type: 'POST',
                            data: {
                                for: 'view',
                                acc_id: accId,
                                id: p_id,
                            },
                            success: function (data) {
                                $('#view').empty();
                                var views = JSON.parse(data);
                                $(views).each(function (key, value) {
                                    $('#view').append('<option value="' + value.id + '">' + value.name + '</option>');
                                });
                            }
                        });
                    }
                });
            }

            $('#account').on('change', function () {
                $('#property').html('<option value="">Loading ...</option>');
                $('#view').html('<option value="">Loading ...</option>');
                var id = $(this).val();
                $.ajax({
                    type: 'POST',
                    data: {
                        for: 'property',
                        id: id
                    },
                    success: function (data) {
                        $('#property').empty();
                        var propertys = JSON.parse(data);
                        $(propertys).each(function (key, value) {
                            $('#property').append('<option value="' + value.id + '" data-acc-id="' + value.acc_id + '">' + value.name + '</option>');
                        });
                        $('#property').trigger('change');
                    }
                });
            });

            $('#property').on('change', function () {
                $('#view').html('<option value="">Loading ...</option>');
                var id = $(this).val();
                var acc_id = $(this).find(':selected').attr('data-acc-id')
                console.log(acc_id);
                $.ajax({
                    type: 'POST',
                    data: {
                        for: 'view',
                        id: id,
                        acc_id: acc_id,
                    },
                    success: function (data) {
                        console.log(data);
                        $('#view').empty();
                        var views = JSON.parse(data);
                        $(views).each(function (key, value) {
                            $('#view').append('<option value="' + value.id + '">' + value.name + '</option>');
                        });
                    }
                });
            });
        });
    </script>

</body>

</html>