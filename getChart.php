<?php
require_once 'connection.php';
require_once 'admin_security.php';

function getDurationFromText($duration)
{
    $arrDate  = [];
    $arrField = [];
    $duration = strtolower($duration);

    if($duration == "today")
    {
        $arrDate['dimension'] = "ga:hour";
        $arrDate['EndDate']   = date('Y-m-d');
        $startDate            = date('Y-m-d');
        for($i = 0; $i <= 23; $i++)
        {
            if($i <= 9)
            {
                $arrField['0' . $i] = $i;
            }
            else
            {
                $arrField[$i] = $i;
            }
        }

        $arrDate['StartDate'] = $startDate;
        $arrDate['arrField']  = $arrField;
    }
    elseif($duration == "yesterday")
    {
        $arrDate['dimension'] = "ga:date";
        $arrDate['EndDate']   = date('Y-m-d');
        $startDate            = date('Y-m-d');
        for($i = 1; $i <= 2; $i++)
        {
            $startDate                                  = date('Y-m-d', strtotime('-1 day', strtotime($startDate)));
            $arrField[date('l', strtotime($startDate))] = date('l', strtotime($startDate));
        }

        $arrDate['StartDate'] = $startDate;
        $arrDate['arrField']  = $arrField;
    }
    elseif($duration == "week" || $duration == "7daysago")
    {
        $arrDate['dimension'] = "ga:dayOfWeekName";
        $arrDate['EndDate']   = date('Y-m-d');
        $startDate            = $arrDate['EndDate'];
        for($i = 1; $i <= 7; $i++)
        {
            $startDate                                  = date('Y-m-d', strtotime('-1 day', strtotime($startDate)));
            $arrField[date('l', strtotime($startDate))] = date('l', strtotime($startDate));
        }
        $arrDate['StartDate'] = $startDate;
        $arrDate['arrField']  = $arrField;
    }
    elseif($duration == "15daysago")
    {
        $arrDate['dimension'] = "ga:date";
        $arrDate['EndDate']   = date('Y-m-d');
        $startDate            = $arrDate['EndDate'];
        for($i = 1; $i <= 15; $i++)
        {
            $arrField[date('Ymd', strtotime($startDate))] = date('d-m-Y', strtotime($startDate));
            $startDate                                    = date('Y-m-d', strtotime('-1 day', strtotime($startDate)));
        }
        $arrField[date('Ymd', strtotime($startDate))] = date('d-m-Y', strtotime($startDate));

        $arrDate['StartDate'] = $startDate;
        $arrDate['arrField']  = $arrField;

    }
    elseif($duration == "month" || $duration == "30daysago")
    {
        $arrDate['dimension'] = "ga:date";
        $arrDate['EndDate']   = date('Y-m-d');
        $startDate            = $arrDate['EndDate'];

        for($i = 1; $i <= 30; $i++)
        {
            $startDate                                    = date('Y-m-d', strtotime('-1 day', strtotime($startDate)));
            $arrField[date('Ymd', strtotime($startDate))] = date('d-m-Y', strtotime($startDate));
        }
        $arrDate['StartDate'] = $startDate;
        $arrDate['arrField']  = $arrField;
    }
    elseif($duration == "year")
    {
        $arrDate['dimension'] = "ga:yearMonth";
        $arrDate['EndDate']   = date('Y-m-d', strtotime('+1 month', time()));
        $startDate            = $arrDate['EndDate'];
        for($i = 1; $i <= 12; $i++)
        {
            $startDate                                   = date('Y-m-d', strtotime('-1 month', strtotime($startDate)));
            $arrField[date('Ym', strtotime($startDate))] = date('F', strtotime($startDate));
        }
        $arrDate['StartDate'] = $startDate;
        $arrDate['arrField']  = $arrField;
    }

    return $arrDate;
}

function printResults($reports)
{
    $arrReport = [];
    for($reportIndex = 0; $reportIndex < count($reports); $reportIndex++)
    {
        $arrReport[$reportIndex] = [];
        $report                  = $reports[$reportIndex];
        $header                  = $report->getColumnHeader();
        $dimensionHeaders        = $header->getDimensions();
        $metricHeaders           = $header->getMetricHeader()->getMetricHeaderEntries();
        $rows                    = $report->getData()->getRows();

        for($rowIndex = 0; $rowIndex < count($rows); $rowIndex++)
        {
            $arrReport[$reportIndex][$rowIndex] = [];
            $row                                = $rows[$rowIndex];
            $dimensions                         = $row->getDimensions();
            $metrics                            = $row->getMetrics();

            $arrReport[$reportIndex][$rowIndex]['dimensionHeaders'] = [];
            for($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++)
            {
                $arrReport[$reportIndex][$rowIndex]['dimensionHeaders'][$dimensionHeaders[$i]] = $dimensions[$i];
            }

            $arrReport[$reportIndex][$rowIndex]['metrics'] = [];
            for($j = 0; $j < count($metrics); $j++)
            {
                $values = $metrics[$j]->getValues();
                for($k = 0; $k < count($values); $k++)
                {
                    $entry                                                            = $metricHeaders[$k];
                    $arrReport[$reportIndex][$rowIndex]['metrics'][$entry->getName()] = $values[$k];
                }
            }
        }
    }

    return $arrReport;
}

function gaDateFormat($date)
{
    $year = substr($date, 0, 4) . '-' . substr($date, 4);
    $date = substr($year, 0, 7) . '-' . substr($year, 7);

    return $date;
}

$db               = new DB();
$ga               = new GA();
$arrResult        = [];
$site             = $db->getAccessToken(['name' => $_POST['site']]);
$arrUsableMetrics = [
    "ga:sessions" => "sessions",
    "ga:users" => "users",
    "ga:newUsers" => "newUsers",
    "ga:pageviews" => "pageviews",
    "ga:pageviewsPerSession" => "pageviewsPerSession",
    "ga:entranceRate" => "entranceRate",
    "ga:percentNewSessions" => "percentNewSessions",
    "ga:exitRate" => "exitRate",
    "ga:bounceRate" => "bounceRate",
    "ga:goalCompletionsAll" => "goalCompletionsAll",
];


if($site['isSuccess'])
{
    $site = $site['objSite'];
    if($_POST['chartType'] == "live_user")
    {
        $arrResult = $ga->getLiveUser($site);
    }
    elseif($_POST['chartType'] == "session_by_device")
    {
        $arrMetrics              = ['ga:sessions' => 'Sessions'];
        $arrParam                = getDurationFromText('month');
        $arrConfig               = [];
        $arrConfig['dimensions'] = ['ga:deviceCategory'];
        $arrConfig['StartDate']  = $arrParam['StartDate'];
        $arrConfig['EndDate']    = $arrParam['EndDate'];
        $analyticsData           = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);
            $arrData         = [];
            // get total
            $total = 0;
            foreach($arrProccessData[0] as $record)
            {
                $total += $record['metrics']['Sessions'];
            }

            foreach($arrProccessData[0] as $record)
            {
                $per                   = ($record['metrics']['Sessions'] * 100) / $total;
                $arrData['label'][]    = ucfirst($record['dimensionHeaders']['ga:deviceCategory']);
                $arrData['datasets'][] = number_format($per, 1);
            }
            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }
    }
    elseif($_POST['chartType'] == "active_pages")
    {
        $arrMetrics              = [
            'ga:users' => 'Users',
            'ga:percentNewSessions' => '% New Sessions',
        ];
        $arrParam                = getDurationFromText('month');
        $arrConfig               = [];
        $arrConfig['dimensions'] = ['ga:pagePath'];
        $arrConfig['StartDate']  = $arrParam['StartDate'];
        $arrConfig['EndDate']    = $arrParam['EndDate'];
        $arrConfig['sort']       = [
            'field' => 'ga:users',
            'order' => 'DESCENDING',
        ];
        $arrConfig['page_size']  = 10;
        $analyticsData           = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);
            $arrData         = [];
            foreach($arrProccessData[0] as $record)
            {
                $arrData[] = [
                    'PageUrl' => $record['dimensionHeaders']['ga:pagePath'],
                    'Users' => number_format($record['metrics']['Users']),
                    'NewSessions' => number_format($record['metrics']['% New Sessions'], 2),
                ];
            }
            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }
    }
    elseif($_POST['chartType'] == "chart_user_type")
    {
        $arrMetrics = ['ga:users' => 'Users'];

        $arrParam = getDurationFromText($_POST['chart_duration']);

        $arrConfig               = [];
        $arrConfig['StartDate']  = $arrParam['StartDate'];
        $arrConfig['EndDate']    = $arrParam['EndDate'];
        $arrConfig['dimensions'] = [
            'ga:userType',
            $arrParam['dimension'],
        ];
        $analyticsData           = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);

            $arrColor['New Visitor']['backgroundColor'] = "transparent";
            $arrColor['New Visitor']['borderColor']     = "#5e72e4";
            $arrColor['New Visitor']['borderWidth']     = 4;
            $arrColor['New Visitor']['tension']         = .4;

            $arrColor['Returning Visitor']['backgroundColor'] = "transparent";
            $arrColor['Returning Visitor']['borderColor']     = "#11cdef";
            $arrColor['Returning Visitor']['borderWidth']     = 4;
            $arrColor['Returning Visitor']['tension']         = .4;

            $arrKeys = [];
            foreach($arrParam['arrField'] as $key => $value)
            {
                $arrKeys[$key] = 0;
            }

            $arrData = [];
            foreach($arrProccessData[0] as $record)
            {
                if(!array_key_exists($record['dimensionHeaders']['ga:userType'], $arrData))
                {
                    $arrData[$record['dimensionHeaders']['ga:userType']]         = [];
                    $arrData[$record['dimensionHeaders']['ga:userType']]['data'] = $arrKeys;
                }
                $arrData[$record['dimensionHeaders']['ga:userType']]['data'][$record['dimensionHeaders'][$arrParam['dimension']]] = intval($record['metrics']['Users']);
            }
            $datasets = [];
            $arrTotal = [];
            foreach($arrData as $key => $data)
            {
                $tmpArray          = [];
                $tmpArray['label'] = $key;
                $tmpArray['data']  = array_reverse(array_values($data['data']));
                $tmpArray          = array_merge($tmpArray, $arrColor[$key]);
                $datasets[]        = $tmpArray;
                $arrTotal[$key]    = number_format(array_sum($data['data']));
            }

            $arrReturn             = [];
            $arrReturn['labels']   = array_reverse(array_values($arrParam['arrField']));
            $arrReturn['datasets'] = $datasets;

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrReturn;
            $arrResult['total']      = $arrTotal;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }
    }
    elseif($_POST['chartType'] == "chart_custom")
    {
        $metrics    = ucfirst(str_replace("ga:", "", $_POST['metrics']));
        $dimension  = ucfirst(str_replace("ga:", "", $_POST['dimension']));
        $arrMetrics = [$_POST['metrics'] => $_POST['metrics']];

        $arrParam = getDurationFromText($_POST['chart_duration']);

        $tmpDate = explode("-", $_POST['chart_duration']);

        $arrConfig               = [];
        $arrConfig['StartDate']  = date('Y-m-d', strtotime($tmpDate[0]));
        $arrConfig['EndDate']    = date('Y-m-d', strtotime($tmpDate[1]));
        $arrConfig['dimensions'] = [$_POST['dimension']];
        $arrConfig['sort']       = [
            'field' => $_POST['metrics'],
            'order' => 'DESCENDING',
        ];
        $arrConfig['page_size']  = 13;

        $analyticsData = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);
            $arrData         = [];
            foreach($arrProccessData[0] as $record)
            {
                $arrData[$record['dimensionHeaders'][$_POST['dimension']]] = intval($record['metrics'][$_POST['metrics']]);
            }

            $arrReturn               = [];
            $arrReturn['labels']     = array_keys($arrData);
            $arrReturn['datasets'][] = [
                'label' => $dimension,
                'data' => array_values($arrData),
                'backgroundColor' => "rgba(94, 113, 228, 1)",
            ];
            $arrResult['data']       = $arrReturn;
            $arrResult['is_success'] = true;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }
    }
    elseif($_POST['chartType'] == "usersChart")
    {
        $arrMetrics = ['ga:users' => 'Users'];

        $arrParam = getDurationFromText($_POST['chart_duration']);

        $arrConfig               = [];
        $arrConfig['StartDate']  = $arrParam['StartDate'];
        $arrConfig['EndDate']    = $arrParam['EndDate'];
        $arrConfig['dimensions'] = [$arrParam['dimension']];
        $analyticsData           = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);

            $arrKeys = [];
            foreach($arrParam['arrField'] as $key => $value)
            {
                $arrKeys[$key] = 0;
            }

            $arrData                  = [];
            $arrData['Users']['data'] = $arrKeys;
            foreach($arrProccessData[0] as $record)
            {
                $arrData['Users']['data'][$record['dimensionHeaders'][$arrParam['dimension']]] = intval($record['metrics']['Users']);
            }

            $datasets = [];
            $arrTotal = [];
            foreach($arrData as $key => $data)
            {
                $tmpArray          = [];
                $tmpArray['label'] = $key;
                $tmpArray['data']  = array_reverse(array_values($data['data']));
                $datasets[]        = $tmpArray;
                $arrTotal[$key]    = number_format(array_sum($data['data']));
            }

            $arrReturn             = [];
            $arrReturn['labels']   = array_reverse(array_values($arrParam['arrField']));
            $arrReturn['datasets'] = $datasets;

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrReturn;
            $arrResult['total']      = $arrTotal;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }

    }
    elseif($_POST['chartType'] == "bounceRateChart")
    {
        $arrMetrics = ['ga:bounceRate' => 'Bounce Rate'];

        $arrParam = getDurationFromText($_POST['chart_duration']);

        $arrConfig               = [];
        $arrConfig['StartDate']  = $arrParam['StartDate'];
        $arrConfig['EndDate']    = $arrParam['EndDate'];
        $arrConfig['dimensions'] = [$arrParam['dimension']];
        $analyticsData           = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);

            $arrKeys = [];
            foreach($arrParam['arrField'] as $key => $value)
            {
                $arrKeys[$key] = 0;
            }

            $arrData                        = [];
            $arrData['Bounce Rate']['data'] = $arrKeys;
            foreach($arrProccessData[0] as $record)
            {
                $arrData['Bounce Rate']['data'][$record['dimensionHeaders'][$arrParam['dimension']]] = intval($record['metrics']['Bounce Rate']);
            }

            $datasets = [];
            $arrTotal = [];
            foreach($arrData as $key => $data)
            {
                $tmpArray          = [];
                $tmpArray['label'] = $key;
                $tmpArray['data']  = array_reverse(array_values($data['data']));
                $datasets[]        = $tmpArray;
                $arrTotal[$key]    = number_format(array_sum($data['data']) / count($data['data']), 2) . "%";
            }

            $arrReturn             = [];
            $arrReturn['labels']   = array_reverse(array_values($arrParam['arrField']));
            $arrReturn['datasets'] = $datasets;

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrReturn;
            $arrResult['total']      = $arrTotal;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }

    }
    elseif($_POST['chartType'] == "sessionDuration")
    {
        $arrMetrics = ['ga:sessionDuration' => 'Session Duration'];

        $arrParam = getDurationFromText($_POST['chart_duration']);

        $arrConfig               = [];
        $arrConfig['StartDate']  = $arrParam['StartDate'];
        $arrConfig['EndDate']    = $arrParam['EndDate'];
        $arrConfig['dimensions'] = [$arrParam['dimension']];
        $analyticsData           = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);

            $arrKeys = [];
            foreach($arrParam['arrField'] as $key => $value)
            {
                $arrKeys[$key] = 0;
            }

            $arrData                             = [];
            $arrData['Session Duration']['data'] = $arrKeys;
            foreach($arrProccessData[0] as $record)
            {
                $arrData['Session Duration']['data'][$record['dimensionHeaders'][$arrParam['dimension']]] = floatval($record['metrics']['Session Duration']);
            }

            $datasets = [];
            $arrTotal = [];
            foreach($arrData as $key => $data)
            {
                $tmpArray          = [];
                $tmpArray['label'] = $key;
                $tmpArray['data']  = array_reverse(array_values($data['data']));
                $datasets[]        = $tmpArray;
                $arrTotal[$key]    = array_sum($data['data']);
            }

            $arrReturn             = [];
            $arrReturn['labels']   = array_reverse(array_values($arrParam['arrField']));
            $arrReturn['datasets'] = $datasets;

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrReturn;
            $arrResult['total']      = $arrTotal;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }

    }
    elseif($_POST['chartType'] == "worldmap")
    {
        $arrMetrics = ['ga:newUsers' => 'Users'];

        $arrConfig               = [];
        $arrConfig['dimensions'] = [
            'ga:latitude',
            'ga:longitude',
            'ga:country',
        ];
        $arrConfig['filter']     = [
            [
                'dimension' => 'ga:latitude',
                'operator' => 'REGEXP',
                'value' => '^[^0]',
            ],
        ];
        $arrConfig['sort']       = [
            'field' => 'ga:newUsers',
            'order' => 'DESCENDING',
        ];
        $arrConfig['page_size']  = 100;

        $analyticsData = $ga->getReport($site, $arrMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);
            $arrData         = [];
            foreach($arrProccessData[0] as $record)
            {
                $location  = $record['dimensionHeaders'];
                $arrData[] = [
                    'latLng' => [
                        floatval($location['ga:latitude']),
                        floatval($location['ga:longitude']),
                    ],
                    'name' => $location['ga:country'] . "(" . $record['metrics']['Users'] . ")",
                ];
            }
            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = $analyticsData['error']->error->message;
        }

    }
    elseif($_POST['chartType'] == 'quick_view')
    {
        $arrCommonMetrics = [
            "ga:avgSessionDuration" => "Session Time",
            "ga:users" => "Users",
            "ga:sessions" => "Sessions",
            "ga:newUsers" => "New Users",
            "ga:pageviews" => "Page Views",
            "ga:bounceRate" => "Bounce Rate",
            "ga:percentNewSessions" => "New Session %",
            "ga:goalCompletionsAll" => "All goals",
            "ga:goalConversionRateAll" => "All Goals CR %",
            "ga:goalValueAll" => "All goals value",
            "ga:totalEvents" => "Total Events",
            "ga:transactions" => "All transactions",
            "ga:transactionRevenue" => "All transaction revenue",
            "ga:transactionRevenuePerSession" => "Avg. revenue per session",
        ];
        $arrMetrics       = [
            $site->top_left => $site->top_left,
            $site->top_right => $site->top_right,
            $site->bottom_left => $site->bottom_left,
            $site->bottom_right => $site->bottom_right,
        ];
        $arrMet           = [
            'top_left' => $site->top_left,
            'top_right' => $site->top_right,
            'bottom_left' => $site->bottom_left,
            'bottom_right' => $site->bottom_right,
        ];

        $arrConfig              = [];
        $arrConfig['StartDate'] = $site->timeframe;
        $arrConfig['EndDate']   = 'today';
        $analyticsData          = $ga->getReport($site, $arrMetrics, $arrConfig);

        // Chart Data
        $arrChartConfig               = [];
        $arrChartMetrics              = [$site->graph => 'Graph'];
        $arrChartParam                = getDurationFromText($site->timeframe);
        $arrChartConfig['StartDate']  = $arrChartParam['StartDate'];
        $arrChartConfig['EndDate']    = $arrChartParam['EndDate'];
        $arrChartConfig['dimensions'] = [$arrChartParam['dimension']];
        $analyticsChartData           = $ga->getReport($site, $arrChartMetrics, $arrChartConfig);
        // End Chart Data

        $arrData        = [];
        $arrChartResult = [];

        if($analyticsData['is_success'])
        {
            $arrProccessData = printResults($analyticsData['data']);
            $data            = $arrProccessData[0][0]['metrics'];

            foreach($arrMet as $key => $value)
            {
                if(strpos($data[$value], '.') !== false)
                {
                    $dataVal = number_format((float)$data[$value], 2, '.', '');
                }
                else
                {
                    $dataVal = $data[$value];
                }

                $arrData[$key] = [$arrCommonMetrics[$value] => $dataVal];
            }

            $arrData['metrics'] = $arrData;
            $site->graph        = $arrCommonMetrics[$site->graph];
            $arrData['site']    = $site;
        }

        if($analyticsChartData['is_success'])
        {
            $arrChartProccessData = printResults($analyticsChartData['data']);

            $arrKeys = [];
            foreach($arrChartParam['arrField'] as $key => $value)
            {
                $arrKeys[$key] = 0;
            }

            $arrChartResult = [];
            $arrChartResult = $arrKeys;

            foreach($arrChartProccessData[0] as $record)
            {
                $arrChartResult[$record['dimensionHeaders'][$arrChartParam['dimension']]] = intval($record['metrics']['Graph']);
            }

            $datasets = [];
            foreach($arrChartResult as $key => $data)
            {
                $datasets[] = $data;
            }

            $arrReturn             = [];
            $arrReturn['labels']   = array_reverse(array_values($arrChartParam['arrField']));
            $arrReturn['datasets'] = array_reverse($datasets);

        }


        if(!$analyticsData['is_success'] || !$analyticsChartData['is_success'])
        {
            $arrResult['is_success'] = false;
            if(!empty($analyticsData['error']->error->message))
            {
                $arrResult['message'] = $analyticsData['error']->error->message;
            }
            elseif($analyticsChartData['error']->error->message)
            {
                $arrResult['message'] = $analyticsChartData['error']->error->message;
            }
        }
        else
        {
            if($analyticsData['is_success'])
            {
                $arrResult['data']['is_success'] = true;
                $arrResult['data']['data']       = $arrData;
            }

            if($analyticsChartData['is_success'])
            {
                $arrResult['chart']['is_success'] = true;
                $arrResult['chart']['data']       = $arrReturn;
            }
        }
    }
    elseif($_POST['actionType'] == "get_card_data")
    {
        if($site->name = $_POST['site'])
        {
            $arrResult['is_success'] = true;
            $arrResult['data']       = $site;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = 'Something Went Wrong.!';
        }
    }
    elseif($_POST['actionType'] == 'set_card_data')
    {
        $formData = [];
        parse_str($_POST['formData'], $formData);
        $upData = $db->update('site', $formData, ['id' => $site->id]);

        if(isset($upData))
        {
            $arrResult['is_success'] = true;
            $arrResult['data']       = $site->id;
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = 'Something Went Wrong.!';
        }
    }
    // Analytics Ajax Call
    elseif($_POST['actionType'] == 'get_analytics_data')
    {
        $dates                   = explode(" - ", $_POST['date']);
        $arrConfig               = [];
        $arrConfig['StartDate']  = date('Y-m-d', strtotime($dates[0]));
        $arrConfig['EndDate']    = date('Y-m-d', strtotime($dates[1]));
        $arrConfig['dimensions'] = ["ga:date"];
        if(isset($_POST['segment']) && !empty($_POST['segment']))
        {
            $arrConfig['segments'] = [$_POST['segment']];
        }

        $analyticsData = $ga->getReport($site, $arrUsableMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrRespData     = $analyticsData['data'][0]->getData()->getTotals();
            $arrProccessData = printResults($analyticsData['data']);
            $arrChartResult  = [];
            $arrData         = array_combine(array_values($arrUsableMetrics), $arrRespData[0]->getValues());

            foreach($arrProccessData[0] as $record)
            {
                foreach($arrUsableMetrics as $met)
                {
                    $key = date("d M Y", strtotime(gaDateFormat($record['dimensionHeaders']['ga:date'])));

                    if(strpos($record['metrics'][$met], '.') !== false)
                    {
                        $arrChartResult[$met][$key] = number_format((float)$record['metrics'][$met], 2, '.', '');
                    }
                    else
                    {
                        $arrChartResult[$met][$key] = $record['metrics'][$met];
                    }
                }
            }

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
            $arrResult['chart']      = $arrChartResult;
        }
    }
    // Audience Ajax Call
    elseif($_POST['actionType'] == 'get_audience_data')
    {
        $dates                   = explode(" - ", $_POST['date']);
        $arrConfig               = [];
        $arrConfig['StartDate']  = date('Y-m-d', strtotime($dates[0]));
        $arrConfig['EndDate']    = date('Y-m-d', strtotime($dates[1]));
        $arrConfig['dimensions'] = [$_POST['dimension']];
        $analyticsData           = $ga->getReport($site, $arrUsableMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrRespData     = $analyticsData['data'][0]->getData()->getTotals();
            $arrProccessData = printResults($analyticsData['data']);
            $arrChartResult  = [];
            $arrData         = array_combine(array_values($arrUsableMetrics), $arrRespData[0]->getValues());

            foreach($arrProccessData[0] as $record)
            {
                foreach($arrUsableMetrics as $met)
                {
                    $key = $record['dimensionHeaders'][$_POST['dimension']];

                    if(strpos($record['metrics'][$met], '.') !== false)
                    {
                        $arrChartResult[$met][$key] = number_format((float)$record['metrics'][$met], 2, '.', '');
                    }
                    else
                    {
                        $arrChartResult[$met][$key] = $record['metrics'][$met];
                    }
                }
            }

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
            $arrResult['chart']      = $arrChartResult;
        }
    }
    // Pages Ajax Call
    elseif($_POST['actionType'] == 'get_pages_data')
    {
        $dates                   = explode(" - ", $_POST['date']);
        $arrConfig               = [];
        $arrConfig['StartDate']  = date('Y-m-d', strtotime($dates[0]));
        $arrConfig['EndDate']    = date('Y-m-d', strtotime($dates[1]));
        $arrConfig['dimensions'] = [
            $_POST['dimension'],
            'ga:date',
        ];
        $analyticsData           = $ga->getReport($site, $arrUsableMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrRespData     = $analyticsData['data'][0]->getData()->getTotals();
            $arrProccessData = printResults($analyticsData['data']);
            $arrChartResult  = [];
            $arrData         = array_combine(array_values($arrUsableMetrics), $arrRespData[0]->getValues());

            foreach($arrProccessData[0] as $record)
            {
                foreach($arrUsableMetrics as $met)
                {
                    $key = date("d M Y", strtotime(gaDateFormat($record['dimensionHeaders']['ga:date'])));

                    if(strpos($record['metrics'][$met], '.') !== false)
                    {
                        $arrChartResult[$met][$key] = number_format((float)$record['metrics'][$met], 2, '.', '');
                    }
                    else
                    {
                        $arrChartResult[$met][$key] = $record['metrics'][$met];
                    }
                }
            }

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
            $arrResult['chart']      = $arrChartResult;
        }
    }
    // SEO Ajax Call
    elseif($_POST['actionType'] == 'get_seo_data')
    {
        $dates                   = explode(" - ", $_POST['date']);
        $arrConfig               = [];
        $arrConfig['StartDate']  = date('Y-m-d', strtotime($dates[0]));
        $arrConfig['EndDate']    = date('Y-m-d', strtotime($dates[1]));
        $arrConfig['dimensions'] = [$_POST['dimension']];
        $analyticsData           = $ga->getReport($site, $arrUsableMetrics, $arrConfig);

        if($analyticsData['is_success'])
        {
            $arrRespData     = $analyticsData['data'][0]->getData()->getTotals();
            $arrProccessData = printResults($analyticsData['data']);
            $arrChartResult  = [];
            $arrData         = array_combine(array_values($arrUsableMetrics), $arrRespData[0]->getValues());

            foreach($arrProccessData[0] as $record)
            {
                foreach($arrUsableMetrics as $met)
                {
                    $key = $record['dimensionHeaders'][$_POST['dimension']];

                    if(strpos($record['metrics'][$met], '.') !== false)
                    {
                        $arrChartResult[$met][$key] = number_format((float)$record['metrics'][$met], 2, '.', '');
                    }
                    else
                    {
                        $arrChartResult[$met][$key] = $record['metrics'][$met];
                    }
                }
            }

            $arrResult['is_success'] = true;
            $arrResult['data']       = $arrData;
            $arrResult['chart']      = $arrChartResult;
        }
    }
    // Widget Ajax Call
    elseif($_POST['actionType'] == 'set_widget_data')
    {
        $formData = [];
        parse_str($_POST['formData'], $formData);
        $formData['site_id'] = $site->id;
        $record              = $db->insert('widget', $formData);
        $widget_id           = $db->lastId();

        if($record)
        {
            $arrResult['is_success'] = true;
            $arrResult['data']       = $site->id;
            $arrResult['widget_id']  = $widget_id;
            $arrResult['title']      = $formData['title'];
        }
        else
        {
            $arrResult['is_success'] = false;
            $arrResult['message']    = 'Something Went Wrong.!';
        }
    }
    elseif($_POST['actionType'] == 'get_widget_data')
    {
        $arrCommonMetrics = [
            "ga:sessions" => "Sessions",
            "ga:users" => "Users",
            "ga:newUsers" => "New Users",
            "ga:pageviews" => "Page Views",
            "ga:pageviewsPerSession" => "Pages/Session",
            "ga:entranceRate" => "Entrance/Page",
            "ga:percentNewSessions" => "% New Sessions",
            "ga:exitRate" => "% Exit",
            "ga:bounceRate" => "Bounce Rate",
            "ga:goalCompletionsAll" => "Goal Completions",
        ];
        $arrWidget        = $db->select('widget', ['id' => $_POST['widget_id']]);
        if($arrWidget['total_record'])
        {
            $arrWidget  = $arrWidget['rs']->fetch_object();
            $arrMetrics = [
                $arrWidget->metrics_1 => $arrWidget->metrics_1,
                $arrWidget->metrics_2 => $arrWidget->metrics_2,
            ];
            $arrMet     = [
                'metrics_1' => $arrWidget->metrics_1,
                'metrics_2' => $arrWidget->metrics_2,
            ];

            $dates                  = explode(" - ", $_POST['date']);
            $arrData                = [];
            $arrConfig              = [];
            $arrConfig['StartDate'] = date('Y-m-d', strtotime($dates[0]));
            $arrConfig['EndDate']   = date('Y-m-d', strtotime($dates[1]));
            $analyticsData          = $ga->getReport($site, $arrMetrics, $arrConfig);


            if($analyticsData['is_success'])
            {
                $arrProccessData = printResults($analyticsData['data']);
                $data            = $arrProccessData[0][0]['metrics'];

                foreach($arrMet as $key => $value)
                {
                    if(strpos($data[$value], '.') !== false)
                    {
                        $dataVal = number_format((float)$data[$value], 2, '.', '');
                    }
                    else
                    {
                        $dataVal = $data[$value];
                    }

                    $arrData[$key] = [$arrCommonMetrics[$value] => $dataVal];
                }
            }

            if($analyticsData['is_success'])
            {
                $arrResult['is_success'] = true;
                $arrResult['data']       = $arrData;
                $arrResult['title']      = $arrWidget->title;
            }
            else
            {
                $arrResult['is_success'] = false;
                $arrResult['data']       = $analyticsData['error']->error->message;
            }
        }
        else
        {
            $arrResult['is_success'] = false;
        }
    }
    elseif($_POST['actionType'] == 'get_widget_for_update')
    {
        $widget = $db->select('widget', ['id' => $_POST['id']]);
        if($widget['total_record'])
        {
            $widget                  = $widget['rs']->fetch_object();
            $arrResult['is_success'] = true;
            $arrResult['data']       = $widget;
        }
        else
        {
            $arrResult['is_success'] = false;
        }
    }
    elseif($_POST['actionType'] == 'update_widget_data')
    {
        $formData = [];
        parse_str($_POST['formData'], $formData);
        $widget_id = $formData['widget_id'];
        unset($formData['widget_id']);

        $widget_update = $db->update('widget', $formData, ['id' => $widget_id]);
        if($widget_update)
        {
            $arrResult['is_success'] = true;
            $arrResult['widget_id']  = $widget_id;
        }
        else
        {
            $arrResult['is_success'] = false;
        }
    }
    elseif($_POST['actionType'] == 'remove_widget')
    {
        $remove_widget = $db->delete('widget', ['id' => $_POST['widget_id']]);

        if($remove_widget)
        {
            $arrResult['is_success'] = true;
        }
        else
        {
            $arrResult['is_success'] = false;
        }
    }
}
else
{
    $arrResult = $site;
}
echo json_encode($arrResult);
die;
?>
