<?php
require_once 'connection.php';

$db = new DB();
$ga = new GA();

require_once 'admin_security.php';

if(isset($siteName))
{
    $site = $db->getAccessToken(['name' => $siteName]);

    if($site['isSuccess'])
    {
        $site = $site['objSite'];
    }
    else
    {
        $error = $site['message'];
    }
}

$arrCommonMet = [
    "ga:sessions" => "Sessions",
    "ga:users" => "Users",
    "ga:newUsers" => "New Users",
    "ga:pageviews" => "Page Views",
    "ga:pageviewsPerSession" => "Pages/Session",
    "ga:entranceRate" => "Entrance/Page",
    "ga:percentNewSessions" => "% New Sessions",
    "ga:exitRate" => "% Exit",
    "ga:bounceRate" => "Bounce Rate",
    "ga:goalCompletionsAll" => "Goal Completions",
];
$recWidget    = $db->select('widget', ['site_id' => $site->id]);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Pages - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
</head>

<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <?php
    if(isset($site))
    { ?>
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 d-inline-block mb-0">Widget Dashboard - <?php echo $site->name; ?></h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links">
                                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo $site->name; ?></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-6 col-5">
                            <div class="form-inline float-right">
                                <div class="input-group mr-sm-2">
                                    <input type="text" name="date_duration" class="form-control date_duration w-100" id="date_duration"/>
                                </div>
                                <button class="btn btn-neutral card_open" data-toggle="modal" data-target="#card_widget">Add Widget</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" value="<?php echo $site->name; ?>" id="site"/>
        <div class="content-wrapper">
            <div class="content container-fluid">
                <section class="page-content">
                    <div>
                        <div class="row" id="load_wrapper">
                            <?php
                            if($recWidget['total_record'])
                            {
                                while($rec = $recWidget['rs']->fetch_object())
                                {
                                  
                                  
                                    ?>
                                    <div class="col-md-3 w_head<?php echo $rec->id; ?>">
                                        <div class="card">
                                            <div class="card-header">
                                                <div class="row">
                                                    <div class="col-6" id="title<?php echo $rec->id; ?>"><?php echo $rec->title; ?></div>
                                                    <div class="col-6 text-right">
                                                        <a href="#" class="widget_name" data-site="<?php echo $site->name; ?>" data-id="<?php echo $rec->id; ?>"><i class="ni ni-settings-gear-65"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-6 text-center">
                                                        <p class="text-lg font-weight-bold mb-1" id="metrics_1<?php echo $rec->id; ?>">0</p>
                                                        <p class="text-sm mb-0" id="metrics_1<?php echo $rec->id; ?>_key"><?php echo $arrCommonMet[$rec->metrics_1]; ?></p>
                                                    </div>
                                                    <div class="col-6 text-center">
                                                        <p class="text-lg font-weight-bold mb-1" id="metrics_2<?php echo $rec->id; ?>">0</p>
                                                        <p class="text-sm mb-0" id="metrics_2<?php echo $rec->id; ?>_key"><?php echo $arrCommonMet[$rec->metrics_2]; ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                            }
                            else
                            { ?>
                                <div class="col-12 pt-4">
                                    <h4 class="h4 text text-center" id="no-widget">No Widget Found.</h4>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 d-inline-block mb-0">Widget Dashboard</h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links">
                                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="content container-fluid">
                <section class="page-content">
                    <div class="col-12">
                        <div class="">
                            <div class="row">
                                <?php
                                $arrSite = $db->select('site');
                                if($arrSite['total_record'])
                                {
                                    while($row = $arrSite['rs']->fetch_object())
                                    { ?>
                                        <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-<?php echo $row->color ?>">
                                            <a href="dashboard.php?site=<?php echo $row->name; ?>">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span class="h2 font-weight-bold mb-0 text-white"><?php echo $row->name; ?></span>
                                                            <h5 class="card-title text-uppercase text-muted mb-0 text-white"><?php echo $row->project_id; ?></h5>
                                                        </div>
                                                        <div class="col-auto">
                                                            <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                                <?php if(!empty($row->icon_class)) { ?>
                                                                    <i class="<?php echo $row->icon_class; ?>"></i>
                                                                <?php } else { ?>
                                                                    <i class="ni ni-ungroup"></i>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-info">
                                    <a href="add_site.php">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <span class="h2 font-weight-bold mb-0 text-white">Add New Site</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                        <i class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<!-- END CONTENT WRAPPER -->

<!--Create Widget-->
<div class="modal fade" id="card_widget" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create Widget</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-1">
                <form method="POST" name="widget_post" id="widget_post">
                    <div class="row">
                        <div class="form-group col-12 mb-3">
                            <label for="title" class="text-sm">Title</label>
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                        <div class="form-group col-12">
                            <label for="metrics_1" class="text-sm">Metrics</label>
                            <select class="form-control selectric" id="metrics_1" name="metrics_1">
                                <?php
                                foreach($arrCommonMet as $key => $val)
                                { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <select class="form-control selectric" id="metrics_2" name="metrics_2">
                                <?php
                                foreach($arrCommonMet as $key => $val)
                                { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary submit_widget">Save</button>
            </div>
        </div>
    </div>
</div>

<!--Edit Widget-->
<div class="modal fade" id="card_widget_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit Widget</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body py-1">
                <form method="POST" name="widget_update" id="widget_update">
                    <div class="row">
                        <div class="form-group col-12 mb-3">
                            <label for="edit_title" class="text-sm">Title</label>
                            <input type="text" name="title" id="edit_title" class="form-control">
                        </div>
                        <div class="form-group col-12">
                            <label for="edit_metrics_1" class="text-sm">Metrics</label>
                            <select class="form-control selectric" id="edit_metrics_1" name="metrics_1">
                                <?php
                                foreach($arrCommonMet as $key => $val)
                                { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                        <input type="hidden" name="widget_id" id="widget_id"/>
                        <div class="form-group col-12">
                            <select class="form-control selectric" id="edit_metrics_2" name="metrics_2">
                                <?php
                                foreach($arrCommonMet as $key => $val)
                                { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $val; ?></option>
                                <?php }
                                ?>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="col-6">
                    <button type="button" class="btn btn-danger delete_widget"><i class="fa fa-trash"></i></button>
                </div>
                <div class="col-6 text-right pr-0">
                    <button type="button" class="btn btn-primary update_widget">Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var url = "<?php echo APP_URL . 'getChart.php' ?>";
</script>
<?php require_once 'footer.php' ?>

<script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="assets/js/chartjs-init.js"></script>

<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>


<script>
    $(document).ready(function () {
        $('input[name = "date_duration"]').daterangepicker({
            opens: 'left',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            dateLimit: {
                'months': 2,
            },
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
        });
    });
</script>

</body>
</html>
