<link rel="icon" href="assets/img/logo/favicon.png" type="image/x-icon">
<!-- Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
<!-- Icons -->
<link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
<link rel="stylesheet" href="assets/vendor/jquery-selectric/selectric.css" type="text/css">
<link rel="stylesheet" href="assets/vendor/@fortawesome/fontawesome-free/css/all.min.css" type="text/css">
<link rel="stylesheet" href="assets/vendor/sweetalert2/dist/sweetalert2.min.css" type="text/css">

<!-- Argon CSS -->
<link rel="stylesheet" href="assets/css/argon.css?v=1.1.0" type="text/css">

 <link rel="stylesheet" href="assets/css/custom.css" type="text/css">
