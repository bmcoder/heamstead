<?php
require_once 'connection.php';

$db = new DB();
$ga = new GA();

require_once 'admin_security.php';

if(isset($siteName))
{
    $site = $db->getAccessToken(['name' => $siteName]);

    if($site['isSuccess'])
    {
        $site = $site['objSite'];
    }
    else
    {
        $error = $site['message'];
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Google Analytics - Multisite | Rajodiya Infotech</title>
    <?php require_once 'head.php'; ?>
    <link rel="stylesheet" href="assets/vendor/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="assets/vendor/bootstrap-daterangepicker/daterangepicker.css">
</head>

<body>
<?php require_once 'sidenav.php'; ?>
<div class="main-content" id="panel">
    <?php require_once 'header.php'; ?>
    <?php
    if(isset($site))
    { ?>
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 d-inline-block mb-0">Analytics Dashboard - <?php echo $site->name; ?></h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links">
                                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item"><a href="dashboard.php">Dashboard</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo $site->name; ?></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" value="<?php echo $site->name; ?>" id="site"/>
        <div class="content-wrapper">
            <div class="content container-fluid">
                <section class="page-content">
                    <div class="row">
                        <div class="col">
                            <div class="card" id="card_custom">
                                <div class="card-header pb-lg-1 pb-md-1">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-7 col-sm-12"><h5 class="h3 mb-0 pt-lg-2 pt-md-2">Custom Chart</h5></div>
                                        <div class="col-lg-6 col-md-5 col-sm-12">
                                            <div class="card-toolbar float-right">
                                                <?php
                                                $arrMatrics                 = [];
                                                $arrMatrics['ga:pageviews'] = [
                                                    'lable' => "Pageviews",
                                                    'dimension' => [
                                                        'ga:userType' => 'User Type',
                                                        'ga:medium' => 'Medium',
                                                        'ga:source' => 'Source',
                                                        'ga:keyword' => 'Keyword',
                                                        'ga:socialNetwork' => 'Social Network',
                                                        'ga:browser' => 'Browser',
                                                        'ga:operatingSystem' => 'Operating System',
                                                        'ga:deviceCategory' => 'Device Category',
                                                        'ga:language' => 'Language',
                                                        'ga:screenResolution' => 'Screen Resolution',
                                                    ],
                                                ];
                                                $arrMatrics['ga:newUsers']  = [
                                                    'lable' => "New Users",
                                                    'dimension' => [
                                                        'ga:userType' => 'User Type',
                                                        'ga:medium' => 'Medium',
                                                        'ga:source' => 'Source',
                                                        'ga:keyword' => 'Keyword',
                                                        'ga:socialNetwork' => 'Social Network',
                                                        'ga:browser' => 'Browser',
                                                        'ga:operatingSystem' => 'Operating System',
                                                        'ga:deviceCategory' => 'Device Category',
                                                        'ga:language' => 'Language',
                                                        'ga:screenResolution' => 'Screen Resolution',
                                                    ],
                                                ];
                                                $arrMatrics['ga:sessions']  = [
                                                    'lable' => "Sessions",
                                                    'dimension' => [
                                                        'ga:userType' => 'User Type',
                                                        'ga:medium' => 'Medium',
                                                        'ga:source' => 'Source',
                                                        'ga:keyword' => 'Keyword',
                                                        'ga:socialNetwork' => 'Social Network',
                                                        'ga:browser' => 'Browser',
                                                        'ga:operatingSystem' => 'Operating System',
                                                        'ga:deviceCategory' => 'Device Category',
                                                        'ga:language' => 'Language',
                                                        'ga:screenResolution' => 'Screen Resolution',
                                                    ],
                                                ];
                                                $arrMatrics['ga:hits']      = [
                                                    'lable' => "Hits",
                                                    'dimension' => [
                                                        'ga:userType' => 'User Type',
                                                        'ga:medium' => 'Medium',
                                                        'ga:source' => 'Source',
                                                        'ga:keyword' => 'Keyword',
                                                        'ga:socialNetwork' => 'Social Network',
                                                        'ga:browser' => 'Browser',
                                                        'ga:operatingSystem' => 'Operating System',
                                                        'ga:deviceCategory' => 'Device Category',
                                                        'ga:language' => 'Language',
                                                        'ga:screenResolution' => 'Screen Resolution',
                                                    ],
                                                ];
                                                ?>
                                                <form class="form-inline text-left" method="post" id="custom_form">
                                                    <label class="sr-only" for="inlineFormInputName2">Metrics</label>
                                                    <div class="mb-2 mr-sm-2">
                                                        <select name="metrics" class="form-control metrics selectric">
                                                            <option value="">Metrics</option>
                                                            <?php foreach($arrMatrics as $key => $matrics) { ?>
                                                                <option data-dimension='<?php echo json_encode($matrics['dimension']); ?>' value="<?php echo $key; ?>"><?php echo $matrics['lable']; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                    <label class="sr-only" for="inlineFormInputName2">Dimension</label>
                                                    <div class="mb-2 mr-sm-2">
                                                        <select name="dimension" class="form-control dimension selectric">
                                                            <option value="">Dimension</option>
                                                        </select>
                                                    </div>
                                                    <label class="sr-only" for="inlineFormInputName2">Duration</label>
                                                    <input type="text" name="dates" class="form-control mb-2 mr-sm-2 duration"/>
                                                    <button type="button" class="btn btn-primary mb-2">Refresh</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body p-0">
                                    <div class="row m-0 col-border-xl">
                                        <div class="col-12 pt-5 text-center" id="empty_custom_chart">
                                            <h4 class="h4">Please Select Metrics & Dimension For Custom Chart</h4>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="card-body" id="container_custom_chart">
                                                <div class="text-center" id="chart_loader" style="display:none;"><img src='assets/img/loader.gif'/></div>
                                                <canvas id="chart_custom"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="header">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 d-inline-block mb-0">Analytics Dashboard</h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links">
                                    <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-wrapper">
            <div class="content container-fluid">
                <section class="page-content">
                    <div class="col-12">
                        <div class="">
                            <div class="row">
                                <?php
                                $arrSite = $db->select('site');
                                if($arrSite['total_record'])
                                {
                                    while($row = $arrSite['rs']->fetch_object())
                                    { ?>
                                        <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-<?php echo $row->color ?>">
                                            <a href="dashboard.php?site=<?php echo $row->name; ?>">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span class="h2 font-weight-bold mb-0 text-white"><?php echo $row->name; ?></span>
                                                            <h5 class="card-title text-uppercase text-muted mb-0 text-white"><?php echo $row->project_id; ?></h5>
                                                        </div>
                                                        <div class="col-auto">
                                                            <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                                <?php if(!empty($row->icon_class)) { ?>
                                                                    <i class="<?php echo $row->icon_class; ?>"></i>
                                                                <?php } else { ?>
                                                                    <i class="ni ni-ungroup"></i>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <div class="col-md-12 col-lg-6 col-xl-3 m-2 card bg-gradient-info">
                                    <a href="add_site.php">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col">
                                                    <span class="h2 font-weight-bold mb-0 text-white">Add New Site</span>
                                                </div>
                                                <div class="col-auto">
                                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                                        <i class="fa fa-plus"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<!-- END CONTENT WRAPPER -->


<script>
    var url = "<?php echo APP_URL . 'getChart.php' ?>";
</script>
<?php require_once 'footer.php' ?>

<!-- ================== PAGE LEVEL SCRIPTS ==================-->

<script src="assets/vendor/chart.js/dist/Chart.min.js"></script>
<script src="assets/vendor/chart.js/dist/Chart.extension.js"></script>
<script src="assets/js/chartjs-init.js"></script>

<!-- ================== DATE SCRIPTS ==================-->
<script src="assets/vendor/moment/min/moment.min.js"></script>
<script src="assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="assets/vendor/bootstrap-daterangepicker/daterangepicker.js"></script>

<script>
    $(document).ready(function () {
        $('input[name="dates"]').daterangepicker({
            opens: 'left',
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
    $(document).on("change", ".metrics", function () {
        var dimension = ($(this).find(':selected').data('dimension'));
        $(".dimension").html('');
        var html = '<option value="">Dimension</option>';
        $.each(dimension, function (k, v) {
            html += '<option value="' + k + '">' + v + '</option>';
        });
        $(".dimension").html(html);

        $('select').selectric('refresh');
    });

</script>

</body>

</html>
